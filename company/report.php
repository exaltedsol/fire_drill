<?php
include_once("../init.php");
if (!isset($_SESSION['COMPANY_ID']) && $_SESSION['COMPANY_ID'] == "") {
    header("location:index.php");
}
if (isset($_GET['mode']) && $_GET['mode'] == "download") {
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment;filename=Tracking_order.csv');
    $heading = '';
    $heading = $heading . ",Zone A,Zone B,Zone C,Zone D\r\n";

    $arr = array();
    for ($zone = 1; $zone <= 4; $zone++) {
        $zone_a = $general_cls_call->select_query("COUNT(id) as total_zone_a", ATTENDANCE_EMPLOYEES, "WHERE building_no=:building_no AND zone=:zone AND drill_id=:drill_id", array(':building_no' => 1, ':zone' => $zone, ':drill_id' => $_GET['drill_id']), 1);
        $arr[] = $zone_a->total_zone_a;
    }
    $heading = $heading . "Fab 2," . $arr[0] . "," . $arr[1] . "," . $arr[2] . "," . $arr[3] . "\r\n";

    $arr1 = array();
    for ($zone = 1; $zone <= 4; $zone++) {
        $zone_a = $general_cls_call->select_query("COUNT(id) as total_zone_a", ATTENDANCE_EMPLOYEES, "WHERE building_no=:building_no AND zone=:zone AND drill_id=:drill_id", array(':building_no' => 2, ':zone' => $zone, ':drill_id' => $_GET['drill_id']), 1);
        $arr1[] = $zone_a->total_zone_a;
    }
    $heading = $heading . "Fab 35," . $arr1[0] . "," . $arr1[1] . "," . $arr1[2] . "," . $arr1[3] . "\r\n";

    $arr2 = array();
    for ($zone = 1; $zone <= 4; $zone++) {
        $zone_a = $general_cls_call->select_query("COUNT(id) as total_zone_a", ATTENDANCE_EMPLOYEES, "WHERE building_no=:building_no AND zone=:zone AND drill_id=:drill_id", array(':building_no' => 3, ':zone' => $zone, ':drill_id' => $_GET['drill_id']), 1);
        $arr2[] = $zone_a->total_zone_a;
    }
    $heading = $heading . "Fab 7," . $arr2[0] . "," . $arr2[1] . "," . $arr2[2] . "," . $arr2[3] . "\r\n";

    $arr3 = array();
    for ($zone = 1; $zone <= 4; $zone++) {
        $zone_a = $general_cls_call->select_query("COUNT(id) as total_zone_a", ATTENDANCE_EMPLOYEES, "WHERE building_no=:building_no AND zone=:zone AND drill_id=:drill_id", array(':building_no' => 4, ':zone' => $zone, ':drill_id' => $_GET['drill_id']), 1);
        $arr3[] = $zone_a->total_zone_a;
    }
    $heading = $heading . "Fab 7G," . $arr3[0] . "," . $arr3[1] . "," . $arr3[2] . "," . $arr3[3] . "\r\n";


    echo $heading;
    exit;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fire Drill</title>
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width, height=device-height, viewport-fit=cover" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!--Include css-->
        <!--<link href="../assets/css/style.css" rel="stylesheet" type="text/css" />-->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
        <!--Bootstrap4 css-->
        <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!--Font Awesome5-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <!--Js Start-->
        <script src="https://momentjs.com/downloads/moment.js"></script>
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            /*function openZone(p) {
                //$("#zone").removeClass('d-none').addClass('d-block');
            }*/
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12 offset-md-4 mb-0">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0 icon_img">
                            <img src="../assets/images/report.jpg" class="img-responsive w-100" alt=""><a href="javascript:void(0)" id="btnExcel"><i class="fas fa-download report"></i></a><a href="logout.php"><i class="fas fa-power-off logout"></i></a>
                        </div>

                        <div class="col-md-12 col-sm-12 col-12 mb-4 drill_border pr-0">
                            <div class="row">
                                <div class="col-md-1 col-sm-1 col-1 drill_border_text" style="padding-left:10px">
                                    <img src="../assets/images/global.png" style="width:20px">
                                </div>
                                <div class="col-md-5 col-sm-5 col-5 drill_border_text text-left pr-0">
                                    <span><a class="drill_text">GLOBAL</a><a class="drill_text1">FOUNDRIES</a></span>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6 drill_border_text pr-0 pl-0">
                                    <span class="drill_text1">Emergency Angle</span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <input type="hidden" name="drill_id" id="drill_id">
                        <div class="col-md-12 col-sm-12 col-12">
                            <p class="heading">Evacuation Duration</p>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="input-group custom_start_time custom_active_time">
                                        <input type="text" class="form-control" name="start_time" id="start_time">
                                        <div class="input-group-prepend" id="startTime">
                                            <span class="input-group-text">Start</span>
                                        </div>
                                        <!--<div class="input-group-append">
                                                <button class="" type="submit">Start</button>  
                                        </div>-->
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="input-group custom_end_time custom_disable_time">
                                        <input type="text" class="form-control" name="end_time" id="end_time">
                                        <div class="input-group-prepend" id="endTime">
                                            <span class="input-group-text">End</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="targetTotalMin">
                    <input type="hidden" id="targetCountMin">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-6 custom_target">
                            <h5>Target</h5>
                            <h6>(mins)</h6>
                            <h3 class="targetMin">0</h3>
                        </div>
                        <div class="col-md-6 col-sm-6 col-6 custom_current">
                            <h5>Current</h5>
                            <h6>(mins)</h6>
                            <h3 class="TotMin">00:00</h3>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 pt-2 pb-2 evacuation_building">
                            <p>Evacuation Building</p>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12">
                                    <div class="default_checkbox_button">
                                        <section id="buildings">
                                            <div>
                                                <!--<input type="checkbox" id="b1" name="building[]" value="1">-->
                                                <label class="doubleLine_radioBtn" id="b1">
                                                    <span>Fab 2</span>
                                                </label>
                                            </div>
                                            <div>
                                                <!--<input type="checkbox" id="b2" name="building[]" value="2">-->
                                                <label class="doubleLine_radioBtn" id="b2">
                                                    <span>Fab 35</span>
                                                </label>
                                            </div>
                                            <div>
                                                <!--<input type="checkbox" id="b3" name="building[]" value="3">-->
                                                <label class="doubleLine_radioBtn" id="b3">
                                                    <span>Fab 7</span>
                                                </label>
                                            </div>
                                            <div>
                                                <!--<input type="checkbox" id="b4" name="building[]" value="4">-->
                                                <label class="doubleLine_radioBtn" id="b4">
                                                    <span>Fab 7G</span>
                                                </label>
                                            </div>
                                            
                                        </section>
                                        <input type="hidden" id="buildingId">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12 mt-3 text-center">
                                    <small>Total # of employee involved</small>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="input-group custom_involved">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Estd. No</span>
                                                </div>
                                                <div class="totalEstd">100000</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-12 mt-3 mb-3 text-center" id="zone">
                                    <h6>Attendance Taken By Zone</h6>
                                    <div class="row">
                                        <div class="col-md-10 col-sm-10 col-10 offset-md-1 offset-sm-1 offset-1 text-left">
                                            <small>Zone A (<span id="za_no">#</span> of attendance taken)</small>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped bg-warning zone-a" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-10 offset-md-1 offset-sm-1 offset-1 text-left">
                                            <small>Zone B (<span id="zb_no">#</span> of attendance taken)</small>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped bg-warning zone-b" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-10 offset-md-1 offset-sm-1 offset-1 text-left">
                                            <small>Zone C (<span id="zc_no">#</span> of attendance taken)</small>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped bg-warning zone-c" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-10 col-sm-10 col-10 offset-md-1 offset-sm-1 offset-1 text-left">
                                            <small>Zone D (<span id="zd_no">#</span> of attendance taken)</small>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped bg-warning zone-d" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="countdown"></div>
    </body>
</html>
<script>
//for report section start
    $(document).ready(function () {
        openZone();
        setInterval(function () {
            openZone();
        }, 3000);
        
        var interval=null;
        //var interval = setInterval(mytimer, 1000);
        $.ajax({
            type: 'post',
            url: '../ajax.php',
            data: {action: 'fetch_drill_id'},
            dataType: 'json',
            success: function (data) {
                $("#drill_id").val(data.id);
                $(".totalEstd").html(data.estimate_no);
                /*var bl = '';
                $.each(data.buildingArr, function(key,value) {
                    var bn = '';
                    if(value.building_id==1) { bn='Fab 2' }
                    if(value.building_id==2) { bn='Fab 35' }
                    if(value.building_id==3) { bn='Fab 7' }
                    if(value.building_id==4) { bn='Fab 7G' }
                    bl += '<div>';
                        bl += '<input type="radio" id="b'+value.building_id+'" name="building[]" value="'+value.building_id+'" onclick="openbuilding('+value.building_id+')">';
                        bl += '<label class="doubleLine_radioBtn" for="b'+value.building_id+'">';
                            bl += '<span>'+bn+'</span>';
                        bl += '</label>';
                    bl += '</div>';
                });
                $("#buildings").html(bl);*/
                $(".targetMin").html(data.target);
                $("#targetTotalMin").val(data.target * 60);
                if ($.trim(data.is_drill_complete) == 1) {
                    /*$("#startTime").addClass('pointer-none');
                    $("#start_time").addClass('pointer-none');*/
                    $(".custom_start_time").removeClass('custom_active_time').addClass('custom_disable_time');
                    $(".custom_end_time").removeClass('custom_disable_time').addClass('custom_active_time');
                    $('#start_time').val(data.start_time);
                    
                    
                    var dStart = $('#start_time').val();
                    var res = dStart.split(":");
                    var endMin = parseInt(res[0]*60)+parseInt(res[1])+parseInt($("#targetTotalMin").val()/60);
                    
                    var dNow = new Date();
                    var hour = (dNow.getHours() < 10) ? '0' + dNow.getHours() : dNow.getHours();
                    var minute = (dNow.getMinutes() < 10) ? '0' + dNow.getMinutes() : dNow.getMinutes();
                    var curTimeInMin = parseInt(hour*60)+parseInt(minute);
                    
                    if(curTimeInMin>endMin) {
                        
                        
                        //var drill_id = $('#drill_id').val();
                        
                        var hourss = Math.floor(endMin / 60);  
                        var hours = (hourss < 10) ? '0' + hourss : hourss;
                        var min = endMin % 60;
                        var minutes = (min < 10) ? '0' + min : min;
    
                        var localEndTime = hours + ':' + minutes;
                        var localEndTimedb = hours + '' + minutes;
                        $.ajax({
                            type: "POST",
                            url: '../ajax.php',
                            data: {'end_time': localEndTimedb, 'drill_id': data.id, action: 'edit_drill_attendance', time_stmp: 'end_time'},
                            dataType: 'json',
                            success: function (data) {
                                
                                //alert(localEndTime);
                                $('#end_time').val(localEndTime);
                                //$(".custom_end_time").addClass('text_disabled');
                                $(".custom_end_time").removeClass('custom_active_time').addClass('custom_disable_time');
                                
                                var mins= Math.floor($("#targetTotalMin").val()/60);
                                var minss = (mins < 10) ? '0' + mins : mins;
                                //$('.TotMin').html(minss + ':00');
                                $('.TotMin').html('00:00');
                                //localTotMin = tothours + ':' + totminutes + ':' + totseconds;

                                //$('.TotMin').html($('.TotMin').html());
                                clearInterval(interval);
                                alert('Time ended');
                            }
                        });
                    }
                    
                    
                    
                    
                    StartTime = data.start_HMS;

                    var dNow = new Date();
                    hour = (dNow.getHours() < 10) ? '0' + dNow.getHours() : dNow.getHours();
                    minutes = (dNow.getMinutes() < 10) ? '0' + dNow.getMinutes() : dNow.getMinutes();
                    seconds = (dNow.getSeconds() < 10) ? '0' + dNow.getSeconds() : dNow.getSeconds();
                    var localStartTimeWithSeconds = hour + ':' + minutes + ':' + seconds;
                    var ms = moment(localStartTimeWithSeconds, "HH:mm:ss").diff(moment(StartTime, "HH:mm:ss"));
                    var d = moment.duration(ms);
                    var diffTime = Math.floor(d.asHours()) + moment.utc(ms).format(":mm:ss");

                    var diffInSec = moment.utc(localStartTimeWithSeconds, 'HH:mm:ss').diff(moment.utc(StartTime, 'HH:mm:ss'), 'seconds');


                    localTotMin = diffTime;
                    totalSeconds = diffInSec;
                    interval = setInterval(mytimer, 1000);
                } else if ($.trim(data.is_drill_complete) == 2) {
                    /*$("#startTime").addClass('pointer-none');
                    $("#start_time").addClass('pointer-none');
                    $('#end_time').addClass('pointer-none');
                    $("#endTime").addClass('pointer-none');*/
                    
                    $(".custom_start_time,.custom_end_time").removeClass('custom_active_time').addClass('custom_disable_time');
                    
                    
                    $('#start_time').val(data.start_time);
                    $('#end_time').val(data.end_time);
                    clearInterval(interval);
                    
                    alert('Time ended');
                } else {
                    localTotMin = '00:00:00';
                    totalSeconds = 0;
                }
            }
        });
        $('#startTime').click(function () {
            var drill_id = $("#drill_id").val();
            //alert(drill_id)
            if($.trim(drill_id)!='')
            {
                if(confirm("Are you sure to start the drill?"))
                {
                    /*$("#startTime").addClass('pointer-none');
                    $("#start_time").addClass('pointer-none');*/
                    /*$(".custom_start_time").addClass('text_disabled');
                    $(".custom_end_time").removeClass('text_disabled');*/
                    
                    $(".custom_start_time").removeClass('custom_active_time').addClass('custom_disable_time');
                    $(".custom_end_time").removeClass('custom_disable_time').addClass('custom_active_time');


                    var dNow = new Date();
                    var month = dNow.getMonth() + 1;
                    var date = dNow.getFullYear() + '-' + month + '-' + dNow.getDate();
                    hour = (dNow.getHours() < 10) ? '0' + dNow.getHours() : dNow.getHours();
                    minutes = (dNow.getMinutes() < 10) ? '0' + dNow.getMinutes() : dNow.getMinutes();
                    seconds = (dNow.getSeconds() < 10) ? '0' + dNow.getSeconds() : dNow.getSeconds();
                    var localStartTime = hour + ':' + minutes;
                    var localStartTimeWithSeconds = hour + ':' + minutes + ':' + seconds;
                    var localStartTimedb = hour + '' + minutes;
                    $('#start_time').val(localStartTime);

                    interval = setInterval(mytimer, 1000);
                   // timer=true;
                    $.ajax({
                        type: "POST",
                        url: '../ajax.php',
                        data: {'start_time': localStartTimedb, 'drill_id': drill_id, action: 'edit_drill_attendance', time_stmp: 'start_time', start_HMS: localStartTimeWithSeconds},
                        dataType: 'json',
                        success: function (data) {

                        }
                    });
                }
            }
            else
            {
                alert('Drill not started.')
            }
        })
        $('#endTime').click(function () {
            clearInterval(interval);
            var drill_id = $('#drill_id').val();
            var dNow = new Date();

            hour = (dNow.getHours() < 10) ? '0' + dNow.getHours() : dNow.getHours();
            minutes = (dNow.getMinutes() < 10) ? '0' + dNow.getMinutes() : dNow.getMinutes();
            seconds = (dNow.getSeconds() < 10) ? '0' + dNow.getSeconds() : dNow.getSeconds();

            var localEndTime = hour + ':' + minutes;
            var localEndTimedb = hour + '' + minutes;
            $.ajax({
                type: "POST",
                url: '../ajax.php',
                data: {'end_time': localEndTimedb, 'drill_id': drill_id, action: 'edit_drill_attendance', time_stmp: 'end_time'},
                dataType: 'json',
                success: function (data) {
                   // timer = false;
                    $('#end_time').val(localEndTime);
                    $(".custom_end_time").removeClass('custom_active_time').addClass('custom_disable_time');

                    //$('.TotMin').html($('.TotMin').html());
                    $('.TotMin').html('00:00');
                    alert('Time ended');
                }
            });
        });

        function mytimer() {
            
                totalSeconds++;
                $("#targetCountMin").val(totalSeconds);

                //For total Minute start
                var tottimer = localTotMin.split(':');
                var tothours = parseInt(tottimer[0], 10);
                var totminutes = parseInt(tottimer[1], 10);
                var totseconds = parseInt(tottimer[2], 10);
                ++totseconds;
                totminutes = (totseconds > 59) ? ++totminutes : totminutes;
                tothours = (totminutes > 59) ? ++tothours : tothours;
                if (totminutes < 0)
                    clearInterval(interval);
                totseconds = (totseconds > 59) ? 0 : totseconds;
                totminutes = (totminutes < 10) ? '0' + totminutes : totminutes;
                totseconds = (totseconds < 10) ? '0' + totseconds : totseconds;
                $('.TotMin').html(totminutes + ':' + totseconds);
                //$('.TotMin').html('00:00');
                localTotMin = tothours + ':' + totminutes + ':' + totseconds;
            
        }
        function chkEnd() {
            var chkTarget = $('#targetTotalMin').val();
            var chkTargetTotal = $('#targetCountMin').val();
            if (parseInt(chkTarget) == parseInt(chkTargetTotal)) {
                clearInterval(chkinterval);
                clearInterval(interval);
                var drill_id = $('#drill_id').val();
                var dNow = new Date();
                hour = (dNow.getHours() < 10) ? '0' + dNow.getHours() : dNow.getHours();
                minutes = (dNow.getMinutes() < 10) ? '0' + dNow.getMinutes() : dNow.getMinutes();
                seconds = (dNow.getSeconds() < 10) ? '0' + dNow.getSeconds() : dNow.getSeconds();

                var localEndTime = hour + ':' + minutes;
                var localEndTimedb = hour + '' + minutes;
                //alert(drill_id);
                $.ajax({
                    type: "POST",
                    url: '../ajax.php',
                    data: {'end_time': localEndTimedb, 'drill_id': drill_id, action: 'edit_drill_attendance', time_stmp: 'end_time'},
                    dataType: 'json',
                    success: function (data) {
                        $('#end_time').val(localEndTime);
                        $(".custom_end_time").removeClass('custom_active_time').addClass('custom_disable_time');

                        //$('.TotMin').html($('.TotMin').html());
                        $('.TotMin').html('00:00');
                        alert('Time ended');
                    }
                });
            }
        }
        var chkinterval = setInterval(chkEnd, 1000);
        //For report page start
        setInterval(function () {
            var buildingId = $("#buildingId").val();
            if (buildingId != "") {
                openZone();
            }
        }, 1000);
        //For report page end
    });
    function openbuilding(p) {
        $("#buildingId").val(p);
    }
    function openZone() {
        //alert(123);
        var drill_id = $("#drill_id").val();
        //var buildingId = $("#buildingId").val();
        if($.trim(drill_id)!='')
        {
            $.ajax({
                type: 'post',
                url: '../report_form_refresh.php',
                data: {'drill_id': drill_id},
                dataType: 'json',
                success: function (data) {
                    //alert(data.zone_a);
                    if ($.trim(data) != '') {
                        //$("#zone").removeClass('d-none').addClass('d-block');
                        $("#za_no").text(data.total_zone_a);
                        $("#zb_no").text(data.total_zone_b);
                        $("#zc_no").text(data.total_zone_c);
                        $("#zd_no").text(data.total_zone_d);
                        $(".doubleLine_radioBtn").removeClass('checked');
                        $.each(data.building_checked, function(key,value) {
                            if(value.building_id==1) { $("#b1").addClass('checked'); }
                            if(value.building_id==2) { $("#b2").addClass('checked'); }
                            if(value.building_id==3) { $("#b3").addClass('checked'); }
                            if(value.building_id==4) { $("#b4").addClass('checked'); }
                        });

                        //$(".totalEstd").html(data.total_employee);
                        $('.zone-a').css({width: data.zone_a + '%'});
                        $('.zone-b').css({width: data.zone_b + '%'});
                        $('.zone-c').css({width: data.zone_c + '%'});
                        $('.zone-d').css({width: data.zone_d + '%'});
                    }
                }
            });
        }
    }
    $('#btnExcel').on('click', (function () {
        var drill_id = $("#drill_id").val();
        window.location.href = 'report.php?mode=download&drill_id='+drill_id;
    }));
//for report section end 
</script>