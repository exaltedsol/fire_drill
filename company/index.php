<?php
    include_once '../init.php';
    $msg = '';
    if (isset($_POST["btnSubmit"])) {
        $chkData = $general_cls_call->select_query("*", USERS, "WHERE employee_id=:employee_id AND company_id=:company_id AND isDeleted=:isDeleted", array(':employee_id'=> $_POST['employee_id'], ':company_id'=>'0',':isDeleted'=>0), 1);
        if(empty($chkData)){
            $msg = '<div class="alert alert-danger alert-dismissible fade show" role="alert" style="color:#fff">
                <strong>Sorry!</strong> Login incorrect.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>';
        }
        else {
            $_SESSION['COMPANY_ID'] = $chkData->id;
            header("location:report.php");
        }
    }
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Fire Drill</title>
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width, height=device-height, viewport-fit=cover" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!--Include css-->
        <!--<link href="../assets/css/style.css" rel="stylesheet" type="text/css" />-->
        <link href="../assets/css/style.css" rel="stylesheet" type="text/css" />
        <!--Bootstrap4 css-->
        <link href="../assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!--Font Awesome5-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <!--Js Start-->
        <script src="../assets/js/jquery.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12 offset-md-4 mb-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0" id="completeSec">
                            <img src="../assets/images/login.jpg" class="img-responsive w-100" alt="">
                        </div>
						
                        <div class="col-md-12 col-sm-12 col-12 mb-4 drill_border pr-0">
                            <div class="row">
                                <div class="col-md-1 col-sm-1 col-1 drill_border_text" style="padding-left:10px">
								<img src="../assets/images/global.png" style="width:20px">
								</div>
								<div class="col-md-5 col-sm-5 col-5 drill_border_text text-left pr-0">
                                    <span><a class="drill_text">GLOBAL</a><a class="drill_text1">FOUNDRIES</a></span>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6 drill_border_text pr-0 pl-0">
                                    <span class="drill_text1">Emergency Angle</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php echo $msg; ?>
                    <form role="form" name="form" method="post" action="" id="fireDrillLogin">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-12 mt-3">
                                <label>Login</label>
                                <div class="form-group custom_input">
                                    <input type="text" class="form-control" name="employee_id" required>
                                </div>
                                <span id="empErr"></span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12 mt-2 text-center">
                                <button type="submit" class="btn btn-primary btn-sm maroon_btn" name="btnSubmit">Submit</button>
                            </div>
                        </div>
                    </form>
                    
                    <!--<div class="row d-none"  id="emsg">
                        <div class="col-md-12 col-sm-12 col-12 mt-5 text-center">
                            <div class="msg_box msg_box_wrong alert-danger">
                                <i class="far fa-times-circle"></i>
                                <p>Sorry! drill not started.</p>
                            </div>
                        </div>
                    </div>
                     <div class="row d-none" id="smsg">
                         <div class="col-md-12 col-sm-12 col-12 mt-5 text-center">
                             <div class="msg_box msg_box_success">
                                 <i class="far fa-check-circle"></i>
                                 <p>Thank you very much.</p>
                             </div>
                         </div>
                     </div>-->


                </div>
            </div>
        </div>
    </body>
</html>