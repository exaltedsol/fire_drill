<?php
	//USING PCONNECT OR NOT :: WRITE 'true' IF YOU ARE USING MYSQL_PCONNET OTHERWISE WRITE 'false'.
	define("USE_PCONNECT", false);

	####### LOCAL CONFIGARATION  #######
	if($_SERVER['HTTP_HOST']=='localhost'){
		define("SERVER_NAME","localhost");
		define("USER_NAME","root");
		define("PASSWORD","");
		define("DATABASE_NAME","exalted_fire_drill");
	}else{
		
		define("SERVER_NAME","localhost");
		define("USER_NAME","exalted_db");
		define("PASSWORD","developer");
		define("DATABASE_NAME","exalted_misc");
	}
	
	
	$database_connection="mysql:host=".SERVER_NAME.";dbname=".DATABASE_NAME;	/* This is the first parameter for establishing a database connection */
	try 
	{
		$db = new PDO($database_connection, USER_NAME, PASSWORD);
   		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e) 
	{
    	die($e->getMessage());													/* This will make a database connection and if not connected it will give error */
	}
	//Domain URL
    define("FOLDER_PATH", "fire_drill");
	define("DOMAIN_SITE", "http://".$_SERVER['HTTP_HOST']."/".FOLDER_PATH);
	define("ADMIN_SITE_URL", DOMAIN_SITE."");
	define("DOCUMENT_ROOT", $_SERVER['DOCUMENT_ROOT'].FOLDER_PATH);
	
	date_default_timezone_set('Asia/Kolkata');
        
        //define folder
        define("INCLUDES", "includes/");
?>