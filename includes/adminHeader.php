<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fire Drill | Control</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome CDN -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"/>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap4.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/all.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!------ menu sortable css---->
  <link rel="stylesheet" href="dist/css/style.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/css/bootstrap-datetimepicker.min.css">
  <!-- Daterange picker -->
  <!-- Custom Tagsinput css Start -->
  <link href="custom_css/tagsinput.css" rel="stylesheet" type="text/css">
  <!-- Custom Tagsinput css End -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- jQuery -->
	<script src="plugins/jquery/jquery.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
	  $.widget.bridge('uibutton', $.ui.button)
	</script>
	<!-- Bootstrap 4 -->
	<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- DataTables -->
	<script src="plugins/datatables/jquery.dataTables.js"></script>
	<script src="plugins/datatables/dataTables.bootstrap4.js"></script>
	<!-- jQuery Knob Chart -->
	<script src="plugins/knob/jquery.knob.js"></script>
	<!-- daterangepicker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="plugins/daterangepicker/daterangepicker.js"></script>
	<!-- datepicker -->
	<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
	<!-- Bootstrap WYSIHTML5 -->
	<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
	<!-- Slimscroll -->
	<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- iCheck 1.0.1 -->
	<script src="plugins/iCheck/icheck.min.js"></script>
	<!-- FastClick -->
	<script src="plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="dist/js/adminlte.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	
	<!-- Custom Tagsinput JS Start -->
	<script src="custom_js/tagsinput.js"></script>
	<!-- Custom Tagsinput JS End -->
	
	<!-- AdminLTE for demo purposes -->
	<!------menu sortable--->
	<script src="dist/js/jquery.nestable.js"></script>
	<script src="dist/js/demo.js"></script>
	<script>
	  $(function () {
		$("#example1").DataTable();
		$('#example2').DataTable({
		  "paging": true,
		  "lengthChange": false,
		  "searching": false,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});
                $("#example3").DataTable({
                    aaSorting: [[0, 'desc']]
                });
	  });
	  
	</script>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
	<!-- Left navbar links -->
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
		</li>
	</ul>
	<!-- Right navbar links -->
	<ul class="navbar-nav ml-auto">
		<!-- Messages Dropdown Menu -->
		<!-- Notifications Dropdown Menu -->
		<li class="nav-item dropdown">
			<a class="nav-link" data-toggle="dropdown" href="#">
				<i class="fa fa-user"></i>
				<span><?php if(isset($_SESSION['ADMIN_USER_NAME']) && $_SESSION['ADMIN_USER_NAME']!='') { echo $_SESSION['ADMIN_USER_NAME']; } else { echo 'Administrator'; } ?> <i class="caret"></i></span>
			</a>
			<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
				<div class="">
					<!--<div class="pull-left hButton">
						<a href="change-password.php" class="btn btn-primary btn-flat">Change Password</a>
					</div>-->
					<div class="hButton">
						<a href="logout.php" class="btn btn-danger btn-flat">Sign out</a>
					</div>
				</div>
			</div>
		</li>
	</ul>
</nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
		<span class="brand-text font-weight-light"><img src="dist/img/AdminLTELogo.png" style="width: 35%; margin-left: 65px;"></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar" style="height: calc(100% - 8rem);">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false"> 
			<!-- Account Start -->
			<li class="nav-item">
				<a href="myaccount.php" class="nav-link <?php echo basename($_SERVER['PHP_SELF'])=='myaccount.php' ? ' active' : ''; ?>">
					<i class="nav-icon fas fa-user"></i>
					<p>My Account</p>
				</a>
			</li>
			<!-- Account End -->
                        <!-- Company Start -->
			<li class="nav-item">
                            <a href="company.php" class="nav-link <?php echo basename($_SERVER['PHP_SELF'])=='company.php' ? ' active' : ''; ?>">
                                <i class="nav-icon fa fa-edit" aria-hidden="true"></i>
                                <p>Edit Company</p>
                            </a>
			</li>
			<!-- Company End -->
			<!-- Add company Start -->	
			<!--<li class="nav-item has-treeview<?php echo (basename($_SERVER['PHP_SELF'])=='company.php' || basename($_SERVER['PHP_SELF'])=='editcompany.php' || basename($_SERVER['PHP_SELF'])=='addcompany.php' || basename($_SERVER['PHP_SELF'])=='addcompany.php') ? ' menu-open' : ''; ?>">
				<a href="#" class="nav-link<?php echo (basename($_SERVER['PHP_SELF'])=='company.php' || basename($_SERVER['PHP_SELF'])=='editcompany.php' || basename($_SERVER['PHP_SELF'])=='addcompany.php' || basename($_SERVER['PHP_SELF'])=='addcompany.php') ? ' active' : ''; ?>">
					<i class="nav-icon far fa-building" aria-hidden="true"></i>
					<p>Company Details<i class="right fa fa-angle-left"></i></p>
				</a>
				<ul class="nav nav-treeview">
					<li class="nav-item">
						<a href="addcompany.php" class="nav-link<?php echo basename($_SERVER['PHP_SELF'])=='addcompany.php' ? ' active' : ''; ?>">
							<i class="nav-icon fa fa-list" aria-hidden="true"></i>
							<p>Add Company</p>
						</a>
					</li>
					<li class="nav-item">
						<a href="company.php" class="nav-link<?php echo basename($_SERVER['PHP_SELF'])=='company.php' ? ' active' : ''; ?>">
							<i class="nav-icon fa fa-list" aria-hidden="true"></i>
							<p>Company List</p>
						</a>
					</li>
				</ul>
			</li>-->
			<!-- Add company End -->
			<!-- file upload Start -->	
			<li class="nav-item">
				<a href="uploadfile.php" class="nav-link <?php echo basename($_SERVER['PHP_SELF'])=='uploadfile.php' ? ' active' : ''; ?>">
					<i class="nav-icon fas fa-cloud-upload-alt"></i>
					<p>Upload File</p>
				</a>
			</li>
			<!-- File upload End -->
			<!-- Employee List Start -->
                        <li class="nav-item">
				<a href="employee.php" class="nav-link <?php echo basename($_SERVER['PHP_SELF'])=='employee.php' ? ' active' : ''; ?>">
					<i class="nav-icon fas fa-users"></i>
					<p>Employees</p>
				</a>
			</li>
			<!-- Employee List End -->
                        <!-- Employee List Start -->
                        <li class="nav-item">
				<a href="drill.php" class="nav-link <?php echo basename($_SERVER['PHP_SELF'])=='drill.php' ? ' active' : ''; ?>">
					<i class="nav-icon fas fa-line-chart"></i>
					<p>Drills</p>
				</a>
			</li>
			<!-- Employee List End -->
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
	</aside>
	<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">