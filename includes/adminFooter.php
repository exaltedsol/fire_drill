</div>
  <!-- /.content-wrapper -->
<footer class="main-footer">
    <strong>Copyright &copy; 2020.</strong>
    A product of Fire Drill.
</footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-----------modal for delete--->
<div class="modal fade" id="confirmDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body">
               Are you sure to delete ?
			   <input type="hidden" id="recordId">
			   <input type="hidden" id="recordTable">
			   
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal" id="deleteConfirm">Delete</button>
            </div>
        </div>
    </div>
</div>
<!--  MODAL FOR delete--->
<script>
	$(document).ready(function(){
		$('[data-toggle="tooltip"]').tooltip(); 
	});
	function deleteData(TABLE, ID) {
		$('#confirmDelete').modal("show");
		$('#recordTable').val(TABLE);
		$('#recordId').val(ID);
			
		$('#deleteConfirm').on('click', function (e) {
		var table 	= $('#recordTable').val();
		var id 		= $('#recordId').val();
		if(table!='' && id!=''){
			var datapost = 'action=deleteRecord&table='+table+'&id='+id;
			$.ajax({
					type: "POST",
					url: "../ajax.php",
					data: datapost,
					success: function(d){
						//alert(d);
					   $("#dataRow"+ID).remove();
					}
				});
			}
		});
	}
        function deleteDataMultiple(TABLE, ID) {
            $('#confirmDelete').modal("show");
            $('#recordTable').val(TABLE);
            $('#recordId').val(ID);

            $('#deleteConfirm').on('click', function (e) {
            var table 	= $('#recordTable').val();
            var id 		= $('#recordId').val();
            if(table!='' && id!=''){
                    var datapost = 'action=deleteRecordMultiple&table='+table+'&id='+id;
                    $.ajax({
                        type: "POST",
                        url: "../ajax.php",
                        data: datapost,
                        success: function(d){
                                //alert(d);
                           $("#dataRow"+ID).remove();
                        }
                    });
                }
            });
	}
</script>
</body>
</html>