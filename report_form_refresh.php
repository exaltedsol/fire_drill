<?php
    include_once("init.php"); 
    ini_set('max_execution_time', 0);
	extract($_POST);
	$data = array();
	//count total user start
	$date = date('Y-m-d');
    //$totalUser = $general_cls_call->select_query("COUNT(id) as tot_emp", USERS, "WHERE user_role=:user_role AND isActive=:isActive AND isDeleted=:isDeleted AND createdDate=:createdDate AND drill_id=:drill_id", array(':user_role'=>0, ':isActive'=>1, ':isDeleted'=>0, ':createdDate'=>$date, ':drill_id'=>$drill_id), 1);
    if($drill_id)
    {
        
        $buildingChecked = $general_cls_call->select_query("building_id", BUILDINGS_PERMISSION, "WHERE drill_id=:drill_id", array(':drill_id'=>$drill_id), 2);
        
    $totalUser = $general_cls_call->select_query("COUNT(id) as tot_emp", ATTENDANCE_EMPLOYEES, "WHERE drill_id=:drill_id AND isActive=:isActive", array(':drill_id'=>$drill_id, ':isActive'=>1), 1);
	//echo $totalUser->tot_emp; exit;
	$total_employee = $totalUser->tot_emp;
	$data['total_employee'] = $total_employee;
	//count total user end
	
	$zone_a = $general_cls_call->select_query("COUNT(id) as total_zone_a", ATTENDANCE_EMPLOYEES, "WHERE zone=:zone AND drill_id=:drill_id AND isActive=:isActive", array(':zone'=>1, ':drill_id'=>$drill_id, ':isActive'=>1), 1);
	$total_zone_a = $zone_a->total_zone_a;
	
	$zone_b = $general_cls_call->select_query("COUNT(id) as total_zone_b", ATTENDANCE_EMPLOYEES, "WHERE zone=:zone AND drill_id=:drill_id AND isActive=:isActive", array(':zone'=>2, ':drill_id'=>$drill_id, ':isActive'=>1), 1);
	$total_zone_b = $zone_b->total_zone_b;
	
	$zone_c = $general_cls_call->select_query("COUNT(id) as total_zone_c", ATTENDANCE_EMPLOYEES, "WHERE zone=:zone AND drill_id=:drill_id AND isActive=:isActive", array(':zone'=>3, ':drill_id'=>$drill_id, ':isActive'=>1), 1);
	$total_zone_c = $zone_c->total_zone_c;
	
	$zone_d = $general_cls_call->select_query("COUNT(id) as total_zone_d", ATTENDANCE_EMPLOYEES, "WHERE zone=:zone AND drill_id=:drill_id AND isActive=:isActive", array(':zone'=>4, ':drill_id'=>$drill_id, ':isActive'=>1), 1);
	$total_zone_d = $zone_d->total_zone_d;
	
	$data['building_checked'] = $buildingChecked;
        
        $data['total_zone_a'] = $total_zone_a;
        $data['zone_a'] = 0;
        if($total_zone_a>0){
            $data['zone_a'] = ($total_zone_a/$total_employee)*100;
        } 
        $data['total_zone_b'] = $total_zone_b;
        $data['zone_b'] = 0;
        if($total_zone_b>0) {
            $data['zone_b'] = ($total_zone_b/$total_employee)*100;
        }
        $data['total_zone_c'] = $total_zone_c;
        $data['zone_c'] = 0;
        if($total_zone_c>0) {
            $data['zone_c'] = ($total_zone_c/$total_employee)*100;
        }
        $data['total_zone_d'] = $total_zone_d;
        $data['zone_d'] = 0;
        if($total_zone_d>0) {
            $data['zone_d'] = ($total_zone_d/$total_employee)*100;
        }
    }
	echo(json_encode($data));
	exit;
?>