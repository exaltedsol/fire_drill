<?php
include_once("init.php"); 
ini_set('max_execution_time', 0);
	$action = $_POST['action'];
	switch($action){
		case "addemployee":
			extract($_POST);
			$data = array();
				$chkExists = $general_cls_call->select_query("id,company_id", USERS, "WHERE employee_id=:employee_id AND isActive=:isActive AND isDeleted=:isDeleted AND company_id!=:company_id", array(':employee_id'=>$employee_id,':isActive'=>1,':isDeleted'=>0,':company_id'=>0), 1);
				if($chkExists)
				{
					$chkEmployee = $general_cls_call->select_query("*", ATTENDANCE_EMPLOYEES, "WHERE employee_id=:employee_id AND drill_id=:drill_id AND company_id=:company_id AND isActive=:isActive", array(':employee_id'=>$employee_id,':drill_id'=>$drill_id,':company_id'=>$chkExists->company_id, ':isActive'=>1), 1);
					
					if($chkEmployee==''){
						$field = "drill_id,building_no,zone,employee_id,company_id,login_time";
						$value = ":drill_id,:building_no,:zone,:employee_id,:company_id,:login_time";
						$addExecute = array(
							':drill_id'	=> $general_cls_call->specialhtmlremover($drill_id),
							':building_no'	=> $general_cls_call->specialhtmlremover($building_no),
							':zone'		=> $general_cls_call->specialhtmlremover($zone),
							':employee_id'	=> $general_cls_call->specialhtmlremover($employee_id),
							':company_id'	=> $chkExists->company_id,
							':login_time'	=> date('Y-m-d H:i:s')
						);
						$addEmploye = $general_cls_call->insert_query(ATTENDANCE_EMPLOYEES, $field, $value, $addExecute);
						if($addEmploye){
							$data['status'] = 200;
							$data['msg'] = '<div class="msg_box msg_box_success text-center" id="sucMsg">
								<i class="far fa-check-circle"></i>
								<p>Thank you very much. You have successfully taken your attendance.</p>
							</div>';
						}else{
							$data['status'] = 400;
							$data['msg'] = '<div class="msg_box msg_box_wrong alert-danger">
								<i class="far fa-times-circle"></i>
								<p>Sorry! submitted wrong.</p>
							</div>';
						}
					}else{
						$data['status'] = 400;
						$data['msg'] = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						Sorry! You have already entry in this drill.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>';
					}
				}
				else
				{
					$data['status'] = 300;
					$data['msg'] = '<div class="alert alert-danger alert-dismissible fade show" role="alert">
						<strong>Sorry!</strong> employee id not matched.
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						  <span aria-hidden="true">&times;</span>
						</button>
					  </div>';
				}
			echo json_encode($data);
		break;
		/*case "add_drill_attendance":
			$field = "start_time,drill_date";
			$value = ":start_time,:drill_date";
			$addExecute = array(
				':drill_date'	=>	$general_cls_call->specialhtmlremover($_POST['drill_date']),
				':start_time'	=>	$general_cls_call->specialhtmlremover($_POST['start_time'])
			);
			$addStartTime = $general_cls_call->insert_query(ATTENDANCE, $field, $value, $addExecute);
			if($addStartTime){
				$data['status'] = 200;
				$data['drill_id'] = $addStartTime;
				$data['msg'] = 'Data inserted successfully';
			}else{
				$data['status'] = 400;
				$data['msg'] = 'Data not insert';
			}
			echo json_encode($data);
		break;*/
		case "edit_drill_attendance":
                    $data = array();
                    if($_POST['drill_id'])
                    {
			if($_POST['time_stmp']=='end_time'){
				$setValues="end_time=:end_time,is_drill_complete=:is_drill_complete";
				$updateExecute=array(
					':end_time'		=> $general_cls_call->specialhtmlremover($_POST['end_time']),
					':is_drill_complete'		=> 2
				);
			}else if($_POST['time_stmp']=='start_time'){
				$setValues="start_time=:start_time,start_HMS=:start_HMS,is_drill_complete=:is_drill_complete";
				$updateExecute=array(
					':start_time'		=> $general_cls_call->specialhtmlremover($_POST['start_time']),
					':start_HMS'		=> $general_cls_call->specialhtmlremover($_POST['start_HMS']),
					':is_drill_complete'		=> 1
				);
			}
			$whereClause=" WHERE id=".$_POST['drill_id'];
			$upEvent = $general_cls_call->update_query(ATTENDANCE, $setValues, $whereClause, $updateExecute);
			if($upEvent){
				$data['status'] = 200;
				$data['msg'] = 'Data updated successfully';
			}else{
				$data['status'] = 400;
				$data['msg'] = 'Data not insert';
			}
                    }
                    echo json_encode($data);
		break;
		case "fetch_drill_id":
			$cur_date = date('Y-m-d');
			$currentDrill = $general_cls_call->select_query("*", ATTENDANCE, "WHERE drill_date=:drill_date ORDER BY id DESC LIMIT 1", array(':drill_date'=>$cur_date), 1);
			
			
			$id = array();
			if($currentDrill){
			
                            $id['id'] = $currentDrill->id;
                            $id['estimate_no'] = $currentDrill->estimate_no;
                            $id['target'] = $currentDrill->target;
                            $id['is_drill_complete'] = $currentDrill->is_drill_complete;
                            $id['start_HMS'] = $currentDrill->start_HMS;
                            if($currentDrill->start_time!=''){
                                    $start = $currentDrill->start_time;
                                    $start_time = str_split($start, 2);
                                    $id['start_time'] = $start_time[0].':'.$start_time[1];
                            }else{
                                    $id['start_time'] = $currentDrill->start_time;
                            }
                            if($currentDrill->end_time!=''){
                                    $end = $currentDrill->end_time;
                                    $end_time = str_split($end, 2);
                                    $id['end_time'] = $end_time[0].':'.$end_time[1];
                            }else{
                                    $id['end_time'] = $currentDrill->end_time;
                            }

                            /*$buildings = $general_cls_call->select_query("building_id", BUILDINGS_PERMISSION, "WHERE drill_id=:drill_id", array(':drill_id'=>$currentDrill->id), 2);
                            $id['buildingArr'] = $buildings;*/
			}
			echo json_encode($id);
		break;
		case "deleteRecord":
			$setValues="isDeleted=:isDeleted";
			$updateExecute=array(':isDeleted'=>1);
			$whereClause=" WHERE id=".$_POST['id'];
			$update = $general_cls_call->update_query($_POST['table'], $setValues, $whereClause, $updateExecute);
			if($update) {
				echo 1;
			}
		break;
                case "deleteRecordMultiple":
                    $tableArr = explode(',',$_POST['table']);
                    foreach($tableArr as $table)
                    {
                        $where = " WHERE drill_id=:drill_id";
                        if($table=='fire_drill_attendance') {
                            $where = " WHERE id=:drill_id";
                        }
                        
                        $general_cls_call->delete_query($table, $where, array(':drill_id'=>$_POST['id']));
                    }
		break;
	}
?>