<!DOCTYPE html>
<html>
    <head>
        <title>Fire Drill</title>
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0, width=device-width, height=device-height, viewport-fit=cover" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <!--Include css-->
        <!--<link href="../assets/css/style.css" rel="stylesheet" type="text/css" />-->
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
        <!--Bootstrap4 css-->
        <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!--Font Awesome5-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

        <!--Js Start-->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script>
            function openZone(p) {
                $("#zone").removeClass('d-none').addClass('d-block');
                $("#alertSec").removeClass('d-block').addClass('d-none');
                $("#evacuateSec").removeClass('d-none').addClass('d-block');
            }
            function chooseZone(p) {
                $("#employee_id").removeClass('d-none').addClass('d-block');
                $("#evacuateSec").removeClass('d-block').addClass('d-none');
                $("#verifySec").removeClass('d-none').addClass('d-block');
            }
            function evacuatBack() {
                $("#zone").removeClass('d-block').addClass('d-none');
                $("#evacuateSec").removeClass('d-block').addClass('d-none');
                $("#alertSec").removeClass('d-none').addClass('d-block');
            }
            function verifyBack() {
                $("#employee_id").removeClass('d-block').addClass('d-none');
                $("#verifySec").removeClass('d-block').addClass('d-none');
                $("#evacuateSec").removeClass('d-none').addClass('d-block');
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-12 offset-md-4 mb-4">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0 d-none" id="completeSec">
                            <img src="assets/images/complete.jpg" class="img-responsive w-100" alt="">
                        </div>
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0" id="alertSec">
                            <img src="assets/images/alert1.jpg" class="img-responsive w-100" alt="">
                        </div>
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0 d-none icon_imge" id="evacuateSec">
                            <img src="assets/images/evacuate.jpg" class="img-responsive w-100" alt=""><i class="fas fa-arrow-left" onclick="evacuatBack()"></i>
                        </div>
                        <div class="col-md-12 col-sm-12 col-12 pl-0 pr-0 d-none icon_imge" id="verifySec">
                            <img src="assets/images/verify.jpg" class="img-responsive w-100" alt=""><i class="fas fa-arrow-left" onclick="verifyBack()"></i>
                        </div>

                        <div class="col-md-12 col-sm-12 col-12 mb-4 drill_border pr-0">
                            <div class="row">
                                <div class="col-md-1 col-sm-1 col-1 drill_border_text" style="padding-left:10px">
								<img src="assets/images/global.png" style="width:20px">
								</div>
								<div class="col-md-5 col-sm-5 col-5 drill_border_text text-left pr-0">
                                    <span><a class="drill_text">GLOBAL</a><a class="drill_text1">FOUNDRIES</a></span>
                                </div>
                                <div class="col-md-6 col-sm-6 col-6 drill_border_text pr-0 pl-0">
                                    <span class="drill_text1">Emergency Angle</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row d-none" id="buildingExists">
                        <div class="col-md-12 col-sm-12 col-12">
                            <label>Are you in '<span id="inBuildings"></span>' building(s)?</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-12 mt-2 text-center">
                            <button class="btn btn-success btn-sm" onclick="openBuildings('1')">Yes</button>
                            <button class="btn btn-danger btn-sm" onclick="openBuildings('2')">No</button>
                            <input type="hidden" id="loadBuildingNo" value="0">
                        </div>
                    </div>
                    
                    <form role="form" name="form" method="post" action="" id="fireDrillMsg">
                        <input type="hidden" name="action" value="addemployee">
                        <input type="hidden" name="drill_id" id="drill_id" value="" class="form-control">
                        
                        
                        
                        
                        <div class="row d-none" id="building">
                            <div class="col-md-12 col-sm-12 col-12">
                                <label>Which building is your office/workstation/workplace?</label>
                                <div class="default_radio_button">
                                    <section id="buildings">
                                        <div>
                                            <input type="radio" id="b1" name="building_no" value="1" onclick="openZone('1')">
                                            <label class="doubleLine_radioBtn" for="b1">
                                                <span>Fab 2</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="b2" name="building_no" value="2" onclick="openZone('2')">
                                            <label class="doubleLine_radioBtn" for="b2">
                                                <span>Fab 35</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="b3" name="building_no" value="3" onclick="openZone('3')">
                                            <label class="doubleLine_radioBtn" for="b3">
                                                <span>Fab 7</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="b4" name="building_no" value="4" onclick="openZone('4')">
                                            <label class="doubleLine_radioBtn" for="b4">
                                                <span>Fab 7G</span>
                                            </label>
                                        </div>
                                    </section>
                                    <input type="hidden" id="loadBuilding" value="0">
                                </div>
                            </div>
                        </div>



                        <div class="row d-none" id="zone">
                            <div class="col-md-12 col-sm-12 col-12 mt-5">
                                <label>Which Assembly Area Zone are you currently assembled?</label>
                                <div class="default_radio_button">
                                    <section>
                                        <div>
                                            <input type="radio" id="z1" name="zone" value="1" onclick="chooseZone('1')">
                                            <label class="doubleLine_radioBtn" for="z1">
                                                <span>A</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="z2" name="zone" value="2" onclick="chooseZone('2')">
                                            <label class="doubleLine_radioBtn" for="z2">
                                                <span>B</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="z3" name="zone" value="3" onclick="chooseZone('3')">
                                            <label class="doubleLine_radioBtn" for="z3">
                                                <span>C</span>
                                            </label>
                                        </div>
                                        <div>
                                            <input type="radio" id="z4" name="zone" value="4" onclick="chooseZone('4')">
                                            <label class="doubleLine_radioBtn" for="z4">
                                                <span>D</span>
                                            </label>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>

                        <div class="row d-none" id="employee_id">
                            <div class="col-md-12 col-sm-12 col-12 mt-5">
                                <label>Please key in your employee ID / RC / SID number (e.g. please key in Exxxxxx, RCxxxxx, or SIDxxxxx)</label>
                                <div class="form-group custom_input">
                                    <input type="text" class="form-control" name="employee_id">
                                </div>
                                <span id="empErr"></span>
                            </div>
                            <div class="col-md-12 col-sm-12 col-12 mt-2 text-center">
                                <button type="submit" class="btn btn-primary btn-sm maroon_btn" name="submit">Submit</button>
                            </div>
                        </div>
                    </form>

                    <div class="row"  id="emsg">
                        <div class="col-md-12 col-sm-12 col-12 mt-5 text-center">
                            <div class="msg_box msg_box_wrong alert-danger">
                                <i class="far fa-times-circle"></i>
                                <p>Sorry! drill not started.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row d-none" id="smsg">
                         <div class="col-md-12 col-sm-12 col-12 mt-5 text-center">
                             <div class="msg_box msg_box_success">
                                 <i class="far fa-check-circle"></i>
                                 <p>Glad that you are not at '<span id="inBuildings2"></span>', please stay away from that building(s). Thank You.</p>
                             </div>
                         </div>
                     </div>


                </div>
            </div>
        </div>
    </body>
</html>

<script type="text/javascript">
    function openBuildings(p)
    {
        $("#buildingExists").removeClass('d-block').addClass('d-none');
        $("#loadBuildingNo").val('1');
        if(p==1){
            $("#building").removeClass('d-none').addClass('d-block');
        } else {
            $("#smsg").removeClass('d-none').addClass('d-block');
        }
    }
    $(document).ready(function () {
        loadlink();
        setInterval(function () {
            loadlink();
        }, 3000);
    });
    function loadlink() {
		
        $.ajax({
            type: 'post',
            url: 'emp_form_refresh.php',
            dataType: 'json',
            success: function (data) {
                if ($.trim(data.drill_id) != '') {
                    $("#drill_id").val(data.drill_id);
                    /*if($("#loadBuilding").val()=='0')
                    {
                        var bl = '';
                        $.each(data.buildingArr, function(key,value) {
                            var bn = '';
                            if(value.building_id==1) { bn='Fab 2' }
                            if(value.building_id==2) { bn='Fab 35' }
                            if(value.building_id==3) { bn='Fab 7' }
                            if(value.building_id==4) { bn='Fab 7G' }
                            bl += '<div>';
                                bl += '<input type="radio" id="b'+value.building_id+'" name="building_no" value="'+value.building_id+'" onclick="openZone('+value.building_id+')">';
                                bl += '<label class="doubleLine_radioBtn" for="b'+value.building_id+'">';
                                    bl += '<span>'+bn+'</span>';
                                bl += '</label>';
                            bl += '</div>';
                            //alert(value.building_id);
                        });
                        $("#buildings").html(bl);
                        $("#loadBuilding").val('1');
                    }*/
    
    
                    var bn = '';
                    $.each(data.buildingArr, function(key,value) {
                        
                        if(value.building_id==1) { bn += 'Fab 2, ' }
                        if(value.building_id==2) { bn += 'Fab 35, ' }
                        if(value.building_id==3) { bn += 'Fab 7, ' }
                        if(value.building_id==4) { bn += 'Fab 7G, ' }
                    });
                    var strVal = $.trim(bn);
                    var lastChar = strVal.slice(-1);
                    if (lastChar == ',') {
                      strVal = strVal.slice(0, -1);
                    }
                    $("#inBuildings, #inBuildings2").text(strVal);
                      
                    if($("#loadBuildingNo").val()=='0')
                    {    
                        $("#emsg").removeClass('d-block').addClass('d-none');
                        $("#buildingExists").removeClass('d-none').addClass('d-block');  
                    }
                    
                } else {
                    $("#buildingExists").removeClass('d-block').addClass('d-none');
                    $("#drill_id").val('');
                    $("#emsg").removeClass('d-none').addClass('d-block');
                    $("#building").removeClass('d-block').addClass('d-none');
                    $("#zone").removeClass('d-block').addClass('d-none');
                    $("#employee_id").removeClass('d-block').addClass('d-none');
                    $("#sucMsg").removeClass('d-block').addClass('d-none');
                }
            }
        });
    }
    $("#fireDrillMsg").submit(function (event) {
        //alert (1);
        event.preventDefault();
        $.ajax({
            type: 'post',
            url: 'ajax.php',
            data: $("#fireDrillMsg").serialize(),
            dataType: 'json',
            cache: false,
            success: function (data) {
                //alert(JSON.stringify(data, null, 4));
                if($.trim(data.status)=='200') {
                    $("#building, #zone, #employee_id, #verifySec").hide();
                    $("#completeSec").show();
                    $("#fireDrillMsg").html(data.msg);
                    $("#verifySec").removeClass('d-block').addClass('d-none');
                    $("#completeSec").removeClass('d-none').addClass('d-block');
                } else {
                    //$("#fireDrillMsg").hide();
                    $("#empErr").html(data.msg);
                    
                }
            }
        });
    });
</script>