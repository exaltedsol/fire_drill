<?PHP
	#######################################################################################
	## THIS FILE IS THE COMMON FILE. THIS FILE INCLUDES ALL THE COMMON FILES REQUIRED IN YOUR WEBSITE. PLEASE INCLUDE THIS FILE IN ALL PAGES. SESSION HAS ALREADY STARTED IN THIS PAGE YOU DO NOT HAVE START SESSION IN THOSE PAGES WHERE YOU HAVE ALREADY INCLUDED THIS PAGE.
	## Created By Exalted Solution
	## Open Source Beta Version
	#######################################################################################
	session_start();
	include("includes/conn.php");
	include("includes/define_tables.php");
	include("includes/queryFunction.php");
        error_reporting(0);
	$general_cls_call=new general($db);
        
        date_default_timezone_set('Asia/Singapore');
        
        //echo date('Y-m-d H:i');
        
        /*$gmtTimezone = new DateTimeZone('GMT');
        $myDateTime = new DateTime(date('Y-m-d H:i'), $gmtTimezone);
        
        $result = $myDateTime->format('Y-m-d H:i:s');
        Echo $result;*/
?>