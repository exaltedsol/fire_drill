-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 19, 2020 at 05:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `exalted_fire_drill`
--

-- --------------------------------------------------------

--
-- Table structure for table `fire_drill_admin_settings`
--

CREATE TABLE `fire_drill_admin_settings` (
  `id` int(11) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `fire_drill_admin_settings`
--

INSERT INTO `fire_drill_admin_settings` (`id`, `email`, `password`) VALUES
(1, 'admin@admin.com', 'c4ca4238a0b923820dcc509a6f75849b');

-- --------------------------------------------------------

--
-- Table structure for table `fire_drill_attendance`
--

CREATE TABLE `fire_drill_attendance` (
  `id` int(11) NOT NULL,
  `drill_date` date NOT NULL,
  `estimate_no` int(10) NOT NULL,
  `target` int(11) NOT NULL DEFAULT 0 COMMENT '(mins)',
  `start_time` varchar(10) DEFAULT NULL,
  `end_time` varchar(10) DEFAULT NULL,
  `is_drill_complete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=Not complete,1=Start,2=End',
  `start_HMS` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=Inactive, 1=Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fire_drill_attendance`
--

INSERT INTO `fire_drill_attendance` (`id`, `drill_date`, `estimate_no`, `target`, `start_time`, `end_time`, `is_drill_complete`, `start_HMS`, `isActive`) VALUES
(1, '2020-03-30', 555, 1, '1925', '', 0, '', 0),
(2, '2020-04-01', 101, 3, '1925', '', 1, '19:25:30', 0),
(3, '2020-03-30', 111, 5, NULL, NULL, 0, NULL, 0),
(4, '2020-03-30', 123, 5, NULL, NULL, 0, NULL, 0),
(5, '2020-03-30', 111, 2, NULL, NULL, 0, NULL, 0),
(6, '2020-03-30', 123, 5, NULL, NULL, 0, NULL, 0),
(7, '2020-04-02', 222, 3, '1923', '', 1, '19:23:32', 0),
(8, '2020-04-02', 101, 3, '2004', '2007', 2, '20:04:29', 0),
(9, '2020-04-02', 125, 3, '2015', '2018', 2, '20:15:37', 0),
(10, '2020-04-06', 45, 1, '2113', '2114', 2, '21:13:54', 0),
(11, '2020-04-09', 141, 2, '1056', '1057', 2, '10:56:24', 0),
(16, '2020-04-10', 111, 2, NULL, NULL, 0, NULL, 0),
(18, '2020-04-16', 99, 3, '1611', '1612', 2, '16:11:19', 0),
(19, '2020-04-23', 1, 1, '1842', '1843', 2, '18:42:14', 0),
(20, '2020-04-23', 1, 1, '1846', '1847', 2, '18:46:36', 0),
(21, '2020-04-23', 1, 1, '1847', '1848', 2, '18:47:50', 0),
(22, '2020-04-23', 1, 1, '1851', '1851', 2, '18:51:24', 0),
(23, '2020-04-23', 1, 1, '1851', '1852', 2, '18:51:49', 0),
(24, '2020-05-04', 102, 20, '1237', '1258', 2, '12:37:40', 0),
(25, '2020-05-04', 3333, 20, '1259', '1259', 2, '12:59:14', 0),
(26, '2020-05-04', 222, 20, '1300', '1302', 2, '13:00:54', 0),
(27, '2020-05-04', 2223, 10, '1304', '1304', 2, '13:04:48', 0),
(28, '2020-05-12', 222, 3, '2024', '2027', 2, '20:24:17', 0),
(29, '2020-05-12', 22, 3, '2053', '2056', 2, '20:53:04', 0),
(30, '2020-05-12', 345, 3, '2057', '2100', 2, '20:57:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `fire_drill_attendance_employees`
--

CREATE TABLE `fire_drill_attendance_employees` (
  `id` int(11) NOT NULL,
  `drill_id` int(11) NOT NULL DEFAULT 0,
  `building_no` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `zone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `employee_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `login_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Active, 0=Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fire_drill_attendance_employees`
--

INSERT INTO `fire_drill_attendance_employees` (`id`, `drill_id`, `building_no`, `zone`, `employee_id`, `company_id`, `login_time`, `isActive`) VALUES
(1, 1, '2', '1', '1', 1, '0000-00-00 00:00:00', 0),
(2, 1, '3', '2', '3', 1, '0000-00-00 00:00:00', 0),
(3, 1, '3', '2', '2', 1, '0000-00-00 00:00:00', 0),
(4, 1, '4', '3', '4', 1, '0000-00-00 00:00:00', 0),
(5, 3, '1', '2', '1', 1, '0000-00-00 00:00:00', 0),
(6, 2, '1', '2', '2', 1, '0000-00-00 00:00:00', 0),
(7, 2, '2', '3', '3', 1, '0000-00-00 00:00:00', 0),
(8, 2, '3', '4', '4', 1, '0000-00-00 00:00:00', 0),
(10, 2, '2', '2', '1', 1, '0000-00-00 00:00:00', 0),
(11, 8, '2', '2', '1234555890', 1, '0000-00-00 00:00:00', 0),
(12, 11, '2', '2', '1236644890', 1, '0000-00-00 00:00:00', 0),
(15, 14, '2', '4', '9886551230', 1, '0000-00-00 00:00:00', 0),
(16, 15, '2', '2', 'sdfs', 1, '0000-00-00 00:00:00', 0),
(17, 18, '2', '4', '8521119630', 1, '2020-04-16 17:36:10', 0),
(18, 18, '2', '3', '9886551230', 1, '2020-04-16 17:36:10', 0),
(19, 18, '1', '2', '1234555890', 1, '2020-04-16 17:36:11', 0),
(20, 18, '1', '2', '1236644890', 1, '2020-04-16 17:36:13', 0),
(21, 18, '2', '2', '1236644890', 1, '2020-04-16 18:41:33', 0),
(22, 18, '2', '3', '9886551230', 1, '2020-04-16 18:41:47', 0),
(23, 18, '2', '1', '8521119630', 1, '2020-04-16 18:42:03', 0),
(24, 18, '2', '2', '2234555890', 1, '2020-04-16 18:42:27', 0),
(25, 26, '2', '2', '8521119630', 1, '2020-05-04 15:31:53', 0),
(26, 28, '2', '3', '8521119630', 1, '2020-05-12 22:55:13', 0),
(27, 28, '4', '4', '9886551230', 1, '2020-05-12 22:55:29', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fire_drill_buildings_permission`
--

CREATE TABLE `fire_drill_buildings_permission` (
  `id` int(10) NOT NULL,
  `drill_id` int(10) NOT NULL DEFAULT 0,
  `building_id` int(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fire_drill_buildings_permission`
--

INSERT INTO `fire_drill_buildings_permission` (`id`, `drill_id`, `building_id`) VALUES
(1, 1, 2),
(2, 1, 3),
(3, 1, 4),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 3, 2),
(8, 3, 3),
(9, 4, 2),
(10, 4, 3),
(11, 5, 2),
(12, 5, 3),
(13, 6, 1),
(14, 6, 2),
(15, 6, 3),
(16, 7, 1),
(17, 7, 2),
(18, 7, 4),
(19, 8, 1),
(20, 8, 2),
(21, 9, 2),
(22, 9, 3),
(23, 10, 1),
(24, 10, 2),
(25, 11, 1),
(31, 14, 1),
(32, 14, 2),
(33, 14, 3),
(34, 14, 4),
(35, 15, 3),
(36, 16, 2),
(37, 16, 3),
(38, 17, 2),
(39, 17, 3),
(40, 18, 2),
(41, 18, 3),
(42, 18, 4),
(43, 19, 2),
(44, 20, 2),
(45, 21, 3),
(46, 22, 2),
(47, 23, 3),
(48, 24, 2),
(49, 24, 3),
(50, 25, 1),
(51, 25, 3),
(52, 26, 2),
(53, 27, 3),
(54, 27, 4),
(55, 28, 2),
(56, 28, 3),
(57, 29, 1),
(58, 29, 2),
(59, 30, 2),
(60, 30, 3);

-- --------------------------------------------------------

--
-- Table structure for table `fire_drill_users`
--

CREATE TABLE `fire_drill_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `company_id` int(11) NOT NULL DEFAULT 0,
  `employee_id` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `drill_id` int(11) NOT NULL DEFAULT 0,
  `user_verified` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0 = Inactive, 1=Active',
  `user_role` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=User,1=Employee,2=Company',
  `isActive` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=Active, 0=Inactive',
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0 COMMENT '1=Deleted, 0=Not Deleted',
  `createdDate` date NOT NULL DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `fire_drill_users`
--

INSERT INTO `fire_drill_users` (`id`, `user_name`, `company_name`, `company_id`, `employee_id`, `drill_id`, `user_verified`, `user_role`, `isActive`, `isDeleted`, `createdDate`) VALUES
(1, 'Company', 'Company', 0, '7003009400', 0, 1, 2, 1, 0, '2020-02-29'),
(28, 'Raju', NULL, 1, '1', 1, 1, 0, 0, 0, '2020-03-25'),
(29, 'Rahim', NULL, 1, '2', 1, 1, 0, 0, 0, '2020-03-25'),
(30, 'Rakesh', NULL, 1, '3', 1, 1, 0, 0, 0, '2020-03-25'),
(31, 'Rupa', NULL, 1, '4', 1, 1, 0, 0, 0, '2020-03-25'),
(32, 'Raju', NULL, 1, '1', 2, 1, 0, 0, 0, '2020-03-30'),
(33, 'Rahim', NULL, 1, '2', 2, 1, 0, 0, 0, '2020-03-30'),
(34, 'Rakesh', NULL, 1, '3', 2, 1, 0, 0, 0, '2020-03-30'),
(35, 'Rupa', NULL, 1, '4', 2, 1, 0, 0, 0, '2020-03-30'),
(36, 'Raju', NULL, 1, '1234555890', 3, 1, 0, 0, 0, '2020-03-30'),
(37, 'Rahim', NULL, 1, '1236644890', 3, 1, 0, 0, 0, '2020-03-30'),
(38, 'Rakesh', NULL, 1, '9886551230', 3, 1, 0, 0, 0, '2020-03-30'),
(39, 'Rupa', NULL, 1, '8521119630', 3, 1, 0, 0, 0, '2020-03-30'),
(40, 'Raju', NULL, 1, '1234555890', 6, 1, 0, 0, 0, '2020-03-30'),
(41, 'Rahim', NULL, 1, '1236644890', 6, 1, 0, 0, 0, '2020-03-30'),
(42, 'Rakesh', NULL, 1, '9886551230', 6, 1, 0, 0, 0, '2020-03-30'),
(43, 'Rupa', NULL, 1, '8521119630', 6, 1, 0, 0, 0, '2020-03-30'),
(44, 'Raju', NULL, 1, '1234555890', 7, 1, 0, 0, 0, '2020-03-30'),
(45, 'Rahim', NULL, 1, '1236644890', 7, 1, 0, 0, 0, '2020-03-30'),
(46, 'Rakesh', NULL, 1, '9886551230', 7, 1, 0, 0, 0, '2020-03-30'),
(47, 'Rupa', NULL, 1, '8521119630', 7, 1, 0, 0, 0, '2020-03-30'),
(48, 'Raju', NULL, 1, '1234555890', 8, 1, 0, 0, 0, '2020-04-02'),
(49, 'Rahim', NULL, 1, '1236644890', 8, 1, 0, 0, 0, '2020-04-02'),
(50, 'Rakesh', NULL, 1, '9886551230', 8, 1, 0, 0, 0, '2020-04-02'),
(51, 'Rupa', NULL, 1, '8521119630', 8, 1, 0, 0, 0, '2020-04-02'),
(52, 'Raju', NULL, 1, '1234555890', 9, 1, 0, 0, 0, '2020-04-02'),
(53, 'Rahim', NULL, 1, '1236644890', 9, 1, 0, 0, 0, '2020-04-02'),
(54, 'Rakesh', NULL, 1, '9886551230', 9, 1, 0, 0, 0, '2020-04-02'),
(55, 'Rupa', NULL, 1, '8521119630', 9, 1, 0, 0, 0, '2020-04-02'),
(56, 'Raju', NULL, 1, '1234555890', 10, 1, 0, 0, 0, '2020-04-02'),
(57, 'Rahim', NULL, 1, '1236644890', 10, 1, 0, 0, 0, '2020-04-02'),
(58, 'Rakesh', NULL, 1, '9886551230', 10, 1, 0, 0, 0, '2020-04-02'),
(59, 'Rupa', NULL, 1, '8521119630', 10, 1, 0, 0, 0, '2020-04-02'),
(60, 'Raju', NULL, 1, '1234555890', 11, 1, 0, 0, 0, '2020-04-09'),
(61, 'Rahim', NULL, 1, '1236644890', 11, 1, 0, 0, 0, '2020-04-09'),
(62, 'Rakesh', NULL, 1, '9886551230', 11, 1, 0, 0, 0, '2020-04-09'),
(63, 'Rupa', NULL, 1, '8521119630', 11, 1, 0, 0, 0, '2020-04-09'),
(72, 'Raju', NULL, 1, '1234555890', 14, 1, 0, 0, 0, '2020-04-09'),
(73, 'Rahim', NULL, 1, '1236644890', 14, 1, 0, 0, 0, '2020-04-09'),
(74, 'Rakesh', NULL, 1, '9886551230', 14, 1, 0, 0, 0, '2020-04-09'),
(75, 'Rupa', NULL, 1, '8521119630', 14, 1, 0, 0, 0, '2020-04-09'),
(76, 'Raju', NULL, 1, '1234555890', 15, 1, 0, 0, 0, '2020-04-10'),
(77, 'Rahim', NULL, 1, 'sdfs', 15, 1, 0, 0, 0, '2020-04-10'),
(78, 'Rakesh', NULL, 1, '9886551230', 15, 1, 0, 0, 0, '2020-04-10'),
(79, 'Rupa', NULL, 1, '8521119630', 15, 1, 0, 0, 0, '2020-04-10'),
(80, 'Raju', NULL, 1, '1234555890', 17, 1, 0, 0, 0, '2020-04-10'),
(81, 'Rahim', NULL, 1, '1236644890', 17, 1, 0, 0, 0, '2020-04-10'),
(82, 'Rakesh', NULL, 1, '9886551230', 17, 1, 0, 0, 0, '2020-04-10'),
(83, 'Rupa', NULL, 1, '8521119630', 17, 1, 0, 0, 0, '2020-04-10'),
(84, 'Raju', NULL, 1, '1234555890', 18, 1, 0, 0, 0, '2020-04-16'),
(85, 'Rahim', NULL, 1, '1236644890', 18, 1, 0, 0, 0, '2020-04-16'),
(86, 'Rakesh', NULL, 1, '9886551230', 18, 1, 0, 0, 0, '2020-04-16'),
(87, 'Rupa', NULL, 1, '8521119630', 18, 1, 0, 0, 0, '2020-04-16'),
(88, 'Raju', NULL, 1, '1234555890', 19, 1, 0, 0, 0, '2020-04-23'),
(89, 'Rahim', NULL, 1, '1236644890', 19, 1, 0, 0, 0, '2020-04-23'),
(90, 'Rakesh', NULL, 1, '9886551230', 19, 1, 0, 0, 0, '2020-04-23'),
(91, 'Rupa', NULL, 1, '8521119630', 19, 1, 0, 0, 0, '2020-04-23'),
(92, 'Raju', NULL, 1, '1234555890', 20, 1, 0, 0, 0, '2020-04-23'),
(93, 'Rahim', NULL, 1, '1236644890', 20, 1, 0, 0, 0, '2020-04-23'),
(94, 'Rakesh', NULL, 1, '9886551230', 20, 1, 0, 0, 0, '2020-04-23'),
(95, 'Rupa', NULL, 1, '8521119630', 20, 1, 0, 0, 0, '2020-04-23'),
(96, 'Raju', NULL, 1, '1234555890', 21, 1, 0, 0, 0, '2020-04-23'),
(97, 'Rahim', NULL, 1, '1236644890', 21, 1, 0, 0, 0, '2020-04-23'),
(98, 'Rakesh', NULL, 1, '9886551230', 21, 1, 0, 0, 0, '2020-04-23'),
(99, 'Rupa', NULL, 1, '8521119630', 21, 1, 0, 0, 0, '2020-04-23'),
(100, 'Raju', NULL, 1, '1234555890', 22, 1, 0, 0, 0, '2020-04-23'),
(101, 'Rahim', NULL, 1, '1236644890', 22, 1, 0, 0, 0, '2020-04-23'),
(102, 'Rakesh', NULL, 1, '9886551230', 22, 1, 0, 0, 0, '2020-04-23'),
(103, 'Rupa', NULL, 1, '8521119630', 22, 1, 0, 0, 0, '2020-04-23'),
(104, 'Raju', NULL, 1, '1234555890', 23, 1, 0, 0, 0, '2020-04-23'),
(105, 'Rahim', NULL, 1, '1236644890', 23, 1, 0, 0, 0, '2020-04-23'),
(106, 'Rakesh', NULL, 1, '9886551230', 23, 1, 0, 0, 0, '2020-04-23'),
(107, 'Rupa', NULL, 1, '8521119630', 23, 1, 0, 0, 0, '2020-04-23'),
(108, 'Raju', NULL, 1, '1234555890', 24, 1, 0, 0, 0, '2020-05-04'),
(109, 'Rahim', NULL, 1, '1236644890', 24, 1, 0, 0, 0, '2020-05-04'),
(110, 'Rakesh', NULL, 1, '9886551230', 24, 1, 0, 0, 0, '2020-05-04'),
(111, 'Rupa', NULL, 1, '8521119630', 24, 1, 0, 0, 0, '2020-05-04'),
(112, 'Raju', NULL, 1, '1234555890', 25, 1, 0, 0, 0, '2020-05-04'),
(113, 'Rahim', NULL, 1, '1236644890', 25, 1, 0, 0, 0, '2020-05-04'),
(114, 'Rakesh', NULL, 1, '9886551230', 25, 1, 0, 0, 0, '2020-05-04'),
(115, 'Rupa', NULL, 1, '8521119630', 25, 1, 0, 0, 0, '2020-05-04'),
(116, 'Raju', NULL, 1, '1234555890', 26, 1, 0, 0, 0, '2020-05-04'),
(117, 'Rahim', NULL, 1, '1236644890', 26, 1, 0, 0, 0, '2020-05-04'),
(118, 'Rakesh', NULL, 1, '9886551230', 26, 1, 0, 0, 0, '2020-05-04'),
(119, 'Rupa', NULL, 1, '8521119630', 26, 1, 0, 0, 0, '2020-05-04'),
(120, 'Raju', NULL, 1, '1234555890', 27, 1, 0, 0, 0, '2020-05-04'),
(121, 'Rahim', NULL, 1, '1236644890', 27, 1, 0, 0, 0, '2020-05-04'),
(122, 'Rakesh', NULL, 1, '9886551230', 27, 1, 0, 0, 0, '2020-05-04'),
(123, 'Rupa', NULL, 1, '8521119630', 27, 1, 0, 0, 0, '2020-05-04'),
(124, 'Raju', NULL, 1, '1234555890', 28, 1, 0, 0, 0, '2020-05-12'),
(125, 'Rahim', NULL, 1, '1236644890', 28, 1, 0, 0, 0, '2020-05-12'),
(126, 'Rakesh', NULL, 1, '9886551230', 28, 1, 0, 0, 0, '2020-05-12'),
(127, 'Rupa', NULL, 1, '8521119630', 28, 1, 0, 0, 0, '2020-05-12'),
(128, 'Raju', NULL, 1, '1234555890', 29, 1, 0, 0, 0, '2020-05-12'),
(129, 'Rahim', NULL, 1, '1236644890', 29, 1, 0, 0, 0, '2020-05-12'),
(130, 'Rakesh', NULL, 1, '9886551230', 29, 1, 0, 0, 0, '2020-05-12'),
(131, 'Rupa', NULL, 1, '8521119630', 29, 1, 0, 0, 0, '2020-05-12'),
(132, 'Raju', NULL, 1, '1234555890', 30, 1, 0, 1, 0, '2020-05-12'),
(133, 'Rahim', NULL, 1, '1236644890', 30, 1, 0, 1, 0, '2020-05-12'),
(134, 'Rakesh', NULL, 1, '9886551230', 30, 1, 0, 1, 0, '2020-05-12'),
(135, 'Rupa', NULL, 1, '8521119630', 30, 1, 0, 1, 0, '2020-05-12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fire_drill_admin_settings`
--
ALTER TABLE `fire_drill_admin_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_drill_attendance`
--
ALTER TABLE `fire_drill_attendance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_drill_attendance_employees`
--
ALTER TABLE `fire_drill_attendance_employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_drill_buildings_permission`
--
ALTER TABLE `fire_drill_buildings_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fire_drill_users`
--
ALTER TABLE `fire_drill_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fire_drill_admin_settings`
--
ALTER TABLE `fire_drill_admin_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fire_drill_attendance`
--
ALTER TABLE `fire_drill_attendance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fire_drill_attendance_employees`
--
ALTER TABLE `fire_drill_attendance_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `fire_drill_buildings_permission`
--
ALTER TABLE `fire_drill_buildings_permission`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `fire_drill_users`
--
ALTER TABLE `fire_drill_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
