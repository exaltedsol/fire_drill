<?php 
	include_once '../init.php';
	ob_start();

	if($_SERVER['REQUEST_METHOD']=="POST" && isset($_POST['btn_AdminSignin']))
	{
		extract($_POST);
		$login = $general_cls_call->select_query("*", SETTINGS, "WHERE email=:email AND password=:password", array(':email'=>$general_cls_call->specialhtmlremover($email),':password'=>md5($general_cls_call->specialhtmlremover($password))), 1);
		if(!empty($login))
		{
			$_SESSION['ADMIN_USER_ID'] = $login->id;
			$_SESSION['ADMIN_USER_NAME'] = $login->email;
			header("location:myaccount.php");
		}
		else
		{
			$ermsg = 'Invalid User ID or Password';
		}
	}
	
	ob_end_flush();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Fire Drill | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="row">
	<div class="col-md-12">
		<div class="login-logo">
			<!--<b>Property </b>Portal-->
			<img src="dist/img/AdminLTELogo.png" style="width:120px; height:120px; margin-top:20px;">
		</div>
	</div>
</div>

  <!-- /.login-logo -->
  <div class="row justify-content-md-center justify-content-sm-center justify-content-center">
      
  <div class="col-md-4 col-sm-5 col-11">
      <div class="login-logo">
			<b>Admin</b>Login
	  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <form action="" method="post" name="frm">
	  <?php 
		if(isset($ermsg) && $ermsg != '') {
	  ?>
		<div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
			<strong><?php echo $ermsg; ?></strong>
		</div>
		<?php 
		}
		?>
        <div class="input-group">
			<input type="text" name="email" class="form-control" placeholder="Email" required>
			<div class="input-group-prepend">
				  <span class="input-group-text input-group-addon rWidth">
					<i class="fa fa-user"></i>
				  </span>
			</div>
    	</div>
        <div class="input-group">
			<input type="password" name="password" class="form-control" placeholder="Password" required>
			<div class="input-group-prepend">
				<span class="input-group-text input-group-addon rWidth">
					<i class="fa fa-lock"></i>
				</span>
			</div>
    	</div>
        <div class="row">
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary btn-block btn-flat" name="btn_AdminSignin">Sign In</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
  </div>
  </div>
<!-- /.login-box -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>