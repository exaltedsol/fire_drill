<?php
	include_once("../init.php");
	//$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('0'));// VALIDATION CHEK
	ob_start();
	//$companyData	=	$general_cls_call->select_query("id,company_name", USERS, "WHERE user_role=:user_role AND isActive=:isActive", array(':isActive'=> 1,':user_role'=>2), 2);
	//header
	include_once("../includes/adminHeader.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee List</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 
	
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<!--<div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Select Company</h3>
                    </div>
                    <form role="form" method="post" action="">
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Choose Company:</label>
										<select name="company_id" class="form-control">
											<option value="">Selected Company</option>
											<?php foreach($companyData as $comData) {?>
											<option value="<?php echo $comData->id;?>"<?php echo $_POST['company_id']==$comData->id ? 'selected':'' ;?>><?php echo $comData->company_name;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
            </div>-->
			<div class="col-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="example1" class="table table-bordered table-striped" style="border-top: 1px solid #dee2e6;">
							<thead>
								<tr>
									<th>Employee Name</th>
									<th>Employee Id</th>
									<th class="text-center" style="width:106px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									//if(isset($_POST['company_id'])) {
										$where = "WHERE isActive=1 AND isDeleted=0 AND user_role=0";
										$sqlQuery = $general_cls_call->select_query("*", USERS, $where, array(), 2);
									//}
									if(!empty($sqlQuery))
									{
										foreach($sqlQuery as $arr)
										{	
								?>
								<tr id="dataRow<?PHP echo $arr->id; ?>">
									<td><?PHP echo $arr->user_name; ?></td>
									<td><?PHP echo $arr->employee_id; ?></td>
									<td class="text-center">
										<a href="editemployee.php?id=<?PHP echo $arr->id; ?>" data-toggle="tooltip" title="Edit" class="editIcon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
										<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" onclick="deleteData('<?PHP echo USERS; ?>', <?PHP echo $arr->id; ?>)" class="delIcon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
    </section>
    <!-- /.content -->
  <!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->	