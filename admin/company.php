<?php
	include_once("../init.php");
	//$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('0'));// VALIDATION CHEK
	ob_start();
	if(isset($_GET['ia']) && ($_GET['ia'] == '1' || $_GET['ia'] == '0'))
	{
		$setValues	="isActive=:isActive";
		$updateExecute=array(
			':isActive' => $general_cls_call->specialhtmlremover($_GET['ia'])
			);
			$whereClause = " WHERE id = ".$_GET['id'];
			$general_cls_call->update_query(USERS, $setValues, $whereClause, $updateExecute);
			header("location:"."company.php");
	}
	//header
	
	$where = "WHERE isDeleted=0";
	
	if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['btnReset']))
	{
		$_POST = array();
	}
	
	$sqlQuery = $general_cls_call->select_query("*", USERS, $where, array(), 2);
	
	include_once("../includes/adminHeader.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Company List</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 
	
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="example1" class="table table-bordered table-striped" style="border-top: 1px solid #dee2e6;">
							<thead>
								<tr>
									<th>Company Name</th>
									<th>Company Id</th>
									<th class="text-center" style="width:106px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$where = "WHERE isDeleted=0 AND user_role=2";
									$sqlQuery = $general_cls_call->select_query("*", USERS, $where, array(), 2);
									
									if(!empty($sqlQuery))
									{
										foreach($sqlQuery as $arr)
										{	
								?>
								<tr id="dataRow<?PHP echo $arr->id; ?>">
									<td><?PHP echo $arr->company_name; ?></td>
									<td><?PHP echo $arr->employee_id; ?></td>
                                                                        <td class="text-center">
										<a href="editcompany.php?id=<?PHP echo $arr->id; ?>" data-toggle="tooltip" title="Edit" class="editIcon"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
						
										<!--<?php if($arr->isActive == '0'){ ?>
											<a href = "company.php?id=<?php echo $arr->id;?>&ia=1" data-toggle="tooltip" title="Click Here To Active" class="inactiveIcon"><i class="fa fa-times"></i></a>
										<?php } else { ?>
											<a href = "company.php?id=<?php echo $arr->id;?>&ia=0" data-toggle="tooltip" title="Click Here To Inactive" class="activeIcon"><i class="fa fa-check"></i></a>
										<?php }	?>
										
										<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" onclick="deleteData('<?PHP echo USERS; ?>', <?PHP echo $arr->id; ?>)" class="delIcon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
									</td>
								</tr>
								<?php
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
    </section>
    <!-- /.content -->
  <!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->	