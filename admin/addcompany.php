<?php
include_once '../init.php';
//$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('0')); // VALIDATION CHEK
ob_start();
	extract($_POST);
	
/* =========== INSERT START =========== */
	if($_SERVER['REQUEST_METHOD'] == "POST" && (isset($_POST['btnSubmit']))){
		
	$selCompany	=	$general_cls_call->select_query("company_name", USERS, "WHERE company_name=:company_name AND employee_id=:employee_id AND isActive=:isActive AND isDeleted=:isDeleted AND user_role=:user_role", array(':company_name'=>$company_name,':employee_id'=>$employee_id,':isActive'=>1,':isDeleted'=>0,':user_role'=>2), 2);
	//echo '<pre>'; print_r($selCompany); exit;
		if(empty($selCompany)){
			$field = "company_name,employee_id,createdDate,user_role,company_id";
			$value = ":company_name,:employee_id,:createdDate,:user_role,:company_id";
			$addExecute = array(
				':company_name' 	=> 	$general_cls_call->specialhtmlremover($company_name),
				':employee_id' 		=> 	$general_cls_call->specialhtmlremover($employee_id),
				':createdDate' 		=> 	date('Y-m-d H:i:s'),
				':user_role' 		=> 	2,
				':company_id' 		=> 	0
			);
			$lastInsertId = $general_cls_call->insert_query(USERS, $field, $value, $addExecute);
			$sucMsg = "Data has been submitted successfully";
			$msgClass = 'success';
		}else{
			$sucMsg = "Data has been already exists!";
			$msgClass = 'danger';
		}
	
	}/*else{
		$sucMsg = "Data has been already exists!";
		$msgClass = 'danger';
	}*/

/* =========== INSERT END =========== */

// ######### HEADER START ############### -->
include_once("../includes/adminHeader.php");
?>
<!-- ######### HEADER END ############### -->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Add Company</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Add Company</h3>
                    </div>
					<?PHP
						if (isset($sucMsg) && $sucMsg != '') {
					?>
                        <div class="alert alert-<?php echo $msgClass; ?> alert-dismissable">
                            <button class="close" data-dismiss="alert">X</button>
                            <i class="fa-fw fa fa-check"></i><strong><?PHP echo $sucMsg; ?></strong> 
                        </div>
                    <?PHP
						}
                    ?>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" method="post" action="" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" name="company_name" placeholder="Company Name" required>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Id</label>
                                        <input type="text" class="form-control" name="employee_id" placeholder="Company Id" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->