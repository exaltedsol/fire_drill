<?php
	include_once("../init.php");
	$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array(1));// VALIDATION CHEK
	ob_start();
        
        
    if (isset($_GET['mode']) && $_GET['mode'] == "download") {
        require_once '../PHPExcel/Classes/PHPExcel.php';
        require_once '../PHPExcel/Classes/PHPExcel/IOFactory.php';
        $objPHPExcel = new PHPExcel();
        // Create a first sheet, representing sales data
        //$objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->removeSheetByIndex(0);
        
        $buildingArr = array('Fab 2'=>1, 'Fab 35'=>2, 'Fab 7'=>3, 'Fab 7G'=>4);
        $c=0;
        $empAttdList = array();
        foreach($buildingArr as $building=>$building_id) {
            
            
            //Add new sheet
            $objWorkSheet = $objPHPExcel->createSheet($c); //Setting index when creating
                
            
            $bR = 'A';
            $zoneArr = array('Zone A'=>1, 'Zone B'=>2, 'Zone C'=>3, 'Zone D'=>4);
            //echo '<pre>';print_r($headingValue);
                
            foreach($zoneArr as $zone=>$zone_id) {
                $start = $bR;
                for($l = 1; $l < 2; $l++) {
                        $bR++;
                }
                $end = $bR;
                    
                //echo $start.':'.$end;exit;
                $objPHPExcel->setActiveSheetIndex($c);
                $objPHPExcel->getActiveSheet($c)->mergeCells($start.'1'.':'.$end.'1');
                $objPHPExcel->getActiveSheet($c)->getCell($start.'1')->setValue($zone);
                $objPHPExcel->getActiveSheet($c)->getStyle($start.'1'.':'.$end.'1')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '0383FC')
                        ),
                        'font'  => array(
                            'bold'  => 500,
                            'color' => array('rgb' => 'ffffff'),
                            'size'  => 12,
                            'name'  => 'Arial'
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'borders' => array(
                            'allborders' => array(
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    )
                );
                                
                $bR++;
                
               
                $headingValue = array("Employee Id","Login Time","Employee Id","Login Time","Employee Id","Login Time","Employee Id","Login Time");
                $hValue = count($headingValue);

                $y = 0;
                $hCol = 'A';
                $hRow = 2;
                for($l = 1; $l <= $hValue; $l++) {
                   // echo $hCol.$hRow;
                    $objPHPExcel->setActiveSheetIndex($c)->setCellValue($hCol.$hRow, $headingValue[$y]);
                    $hCol++;
                    $y++;
                }
                $objPHPExcel->getActiveSheet($c)->getStyle('A2:H2')->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => 'f5b678')
                        ),
                        'font'  => array(
                            'bold'  => 500,
                            'color' => array('rgb' => '000000'),
                            'size'  => 11,
                            'name'  => 'Arial'
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        ),
                        'borders' => array(
                            'allborders' => array(
                              'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    )
                );
                
                $empAttdList = $general_cls_call->select_query("*", ATTENDANCE_EMPLOYEES, "WHERE building_no=:building_id AND zone=:zone_id AND drill_id=:drill_id", array(':building_id'=>$building_id, ':drill_id'=>$_GET['drill_id'], ':zone_id'=>$zone_id), 2);
            //echo '<pre>';print_r($empAttdList);echo '</pre>';
                
                $row = 3; // 1-based index
                $col = 0;
                foreach($empAttdList as $emp) {
                    $col = ($emp->zone-1)*2;
                    
                    $objPHPExcel->getActiveSheet($c)->setCellValueByColumnAndRow($col, $row, $emp->employee_id);
                    $col++;
                    $objPHPExcel->getActiveSheet($c)->setCellValueByColumnAndRow($col, $row, date('d/m/Y H:i:s', strtotime($emp->login_time)));
                    $col++;
                    $objPHPExcel->getActiveSheet($c)->getStyle('A'.$row.':H'.$row)->applyFromArray(
                        array(
                            'alignment' => array(
                                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            )
                        )
                    );
                    $row++;
                    
                    
                }
                
            }
            
            //echo 1;exit;
            //Rename sheet
            $objWorkSheet->setTitle($building);
            $c++;
        }
        //exit;
        
        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$_GET['drill_id'].'_'.date('Y-m-d H:i:s').'.xls"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
	//header
	include_once("../includes/adminHeader.php");
?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee List</h1>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section> 
	
    <!-- Main content -->
    <section class="content">
		<div class="row">
			<!--<div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Select Company</h3>
                    </div>
                    <form role="form" method="post" action="">
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Choose Company:</label>
										<select name="company_id" class="form-control">
											<option value="">Selected Company</option>
											<?php foreach($companyData as $comData) {?>
											<option value="<?php echo $comData->id;?>"<?php echo $_POST['company_id']==$comData->id ? 'selected':'' ;?>><?php echo $comData->company_name;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Search</button>
                        </div>
                    </form>
                </div>
            </div>-->
			<div class="col-12">
				<div class="card">
					<!-- /.card-header -->
					<div class="card-body table-responsive">
						<table id="example3" class="table table-bordered table-striped" style="border-top: 1px solid #dee2e6;">
							<thead>
								<tr>
									<th>Drill Date</th>
									<th>Estimate No</th>
                                                                        <th>Target</th>
									<th class="text-center" style="width:106px">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									//if(isset($_POST['company_id'])) {
										$where = "WHERE is_drill_complete=2 ORDER BY drill_date DESC";
										$sqlQuery = $general_cls_call->select_query("*", ATTENDANCE, $where, array(), 2);
									//}
									if(!empty($sqlQuery))
									{
										foreach($sqlQuery as $arr)
										{	
								?>
                                                            <tr id="dataRow<?PHP echo $arr->id; ?>">
                                                                    <td>
                                                                        <span style="display:none"><?PHP echo strtotime($arr->drill_date.' '.$arr->start_HMS); ?></span>
                                                                        <?PHP echo date('d/m/Y', strtotime($arr->drill_date)).' '.$arr->start_HMS; ?>
                                                                    </td>
									<td><?PHP echo $arr->estimate_no; ?></td>
									<td><?PHP echo $arr->target; ?></td>
									<td class="text-center">
										<a href="drill.php?mode=download&drill_id=<?PHP echo $arr->id; ?>" id="btnExcel" data-toggle="tooltip" title="Export" class="editIcon"><i class="fas fa-download report"></i></a>
										<a href="javascript:void(0)" data-toggle="tooltip" title="Delete" onclick="deleteDataMultiple('<?PHP echo ATTENDANCE.','.ATTENDANCE_EMPLOYEES.','.BUILDINGS_PERMISSION.','.USERS; ?>', <?PHP echo $arr->id; ?>)" class="delIcon"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
									</td>
								</tr>
								<?php
										}
									}
								?>
							</tbody>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
    </section>
    <!-- /.content -->
  <!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->	