<?php
include_once '../init.php';
include('../' . INCLUDES . "simplexlsx.class.php");
$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('1')); // VALIDATION CHEK
ob_start();
$companyData = $general_cls_call->select_query("id,company_name", USERS, "WHERE user_role=:user_role AND isActive=:isActive AND isDeleted=:isDeleted", array(':isActive' => 1, ':user_role' => 2, ':isDeleted' => 0), 2);

//echo '<pre>'; print_r($companyData); echo '</pre>';exit;
/* =========== INSERT START =========== */
if (isset($_POST["btnSubmit"])) {

    $setUValuesA = "isActive=:isActive";
    $arrayUExeA = array(
        ':isActive' => '0'
    );
    $whereUClauseA = " WHERE 1";
    $general_cls_call->update_query(ATTENDANCE, $setUValuesA, $whereUClauseA, $arrayUExeA);
                
    $field = "estimate_no,target,drill_date";
    $value = ":estimate_no,:target,:drill_date";
    $dataArray = array(
        ':estimate_no' => $_POST['estimate_no'],
        ':target' => $_POST['target'],
        ':drill_date' => date('Y-m-d')
    );
    $attendance = $general_cls_call->insert_query(ATTENDANCE, $field, $value, $dataArray);
    foreach($_POST['building'] as $building_id) {
        $field2 = "drill_id,building_id";
        $value2 = ":drill_id,:building_id";
        $dataArray2 = array(
            ':drill_id' => $attendance,
            ':building_id' => $building_id
        );
        $general_cls_call->insert_query(BUILDINGS_PERMISSION, $field2, $value2, $dataArray2);
    }
    if ($attendance) {
        $fileName = $_FILES["csvfile"]["tmp_name"];
        $chkFilename = $_FILES["csvfile"]["name"];
        $ext = explode('.', $chkFilename);
        if (!empty($chkFilename)) {
            if ($_FILES["csvfile"]["size"] > 0) {
                //remove previous data respect to company id
                /*$general_cls_call->delete_query(ATTENDANCE_EMPLOYEES, "WHERE company_id=:company_id", array(':company_id' => 1));
                $general_cls_call->delete_query(USERS, "WHERE company_id=:company_id", array(':company_id' => 1));*/
                
                $setUValues = "isActive=:isActive";
                $arrayUExe = array(
                    ':company_id' 			=> 1,
                    ':isActive' 			=> '0'
                );
                $whereUClause = " WHERE company_id=:company_id";
                $general_cls_call->update_query(ATTENDANCE_EMPLOYEES, $setUValues, $whereUClause, $arrayUExe);
                
                $setUValues = "isActive=:isActive";
                $arrayUExe = array(
                    ':company_id' 			=> 1,
                    ':isActive' 			=> '0'
                );
                $whereUClause = " WHERE company_id=:company_id";
                $general_cls_call->update_query(USERS, $setUValues, $whereUClause, $arrayUExe);
    

                if ($ext[1] == 'xls' || $ext[1] == 'xlsx') {
                    $xlsx = new SimpleXLSX($fileName);

                    if ($xlsx->success()) {
                        list($cols, $rows) = $xlsx->dimension();
                        $i = 0;
                        $rArr = $xlsx->rows();
                        //echo '<pre>';print_r($rArr);echo '</pre>';exit;
                        foreach ($rArr as $k => $r) {
                            if ($i > 0 && $r[0] != '') {
                                $name = $r[0];
                                $employee_id = $r[1];
                                
                                if($employee_id!='')
                                {
                                    $chkData = $general_cls_call->select_query("*", USERS, "WHERE employee_id=:employee_id AND company_id=:company_id AND isActive=:isActive", array(':employee_id' => $employee_id, ':company_id' => 1, ':isActive' => 1), 2);
                                    if (empty($chkData)) {
                                        $fields = "user_name,employee_id,drill_id,company_id,createdDate";
                                        $values = ":user_name,:employee_id,:drill_id,:company_id,:createdDate";
                                        //':company_id' => $_POST["company_id"],
                                        $data_array = array(
                                            ':user_name' => $name,
                                            ':employee_id' => $employee_id,
                                            ':drill_id' => $attendance,
                                            ':company_id' => 1,
                                            ':createdDate' => date('Y-m-d')
                                        );
                                        $sqlInsert = $general_cls_call->insert_query(USERS, $fields, $values, $data_array);
                                        //echo '<pre>'; print_r($sqlInsert); echo '</pre>';
                                        $sucMsg = "Data Imported into the Database";
                                        $msgClass = 'success';
                                    }
                                     else {
                                        $sucMsg = "Problem in Importing Data";
                                        $msgClass = 'danger';
                                    }
                                }
                                //echo $name.'--'.$employee_id.'--'.$email.'<br/>';
                            }
                            $i++;
                        }
                    }
                } else {
                    $fileCsv = fopen($fileName, "r");
                    $i = 1;
                    while (($column = fgetcsv($fileCsv, 10000, ",")) !== FALSE) {
                        //echo '<pre>'; print_r($column); echo '</pre>';
                        //exit;
                        if($column[1]!='')
                        {
                            $chkData = $general_cls_call->select_query("*", USERS, "WHERE employee_id=:employee_id AND company_id=:company_id AND isActive=:isActive", array(':employee_id' => $column[1], ':company_id' => 1, ':isActive' => 1), 2);
                            //echo $column[1];exit;
                            if (empty($chkData)) {
                                if ($i > 1) {
                                    $fields = "user_name,employee_id,drill_id,company_id,createdDate";
                                    $values = ":user_name,:employee_id,:drill_id,:company_id,:createdDate";
                                    $data_array = array(
                                        ':user_name' => $column[0],
                                        ':employee_id' => $column[1],
                                        ':drill_id' => $attendance,
                                        ':company_id' => 1,
                                        ':createdDate' => date('Y-m-d')
                                    );
                                    $sqlInsert = $general_cls_call->insert_query(USERS, $fields, $values, $data_array);
                                    //echo '<pre>'; print_r($sqlInsert); echo '</pre>';
                                    $sucMsg = "Data Imported into the Database";
                                    $msgClass = 'success';
                                } else {
                                    $sucMsg = "Problem in Importing Data";
                                    $msgClass = 'danger';
                                }
                                $i++;
                            }
                        }
                    }
                }
            }
        }
    }
}
if (isset($_GET['mode']) && $_GET['mode'] == 'download') {
    $filepath = 'demo_csv/demo.csv';

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . basename($filepath) . '"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($filepath));
    flush(); // Flush system output buffer
    readfile($filepath);
    exit;
}
/* =========== INSERT END =========== */

// ######### HEADER START ############### -->
include_once("../includes/adminHeader.php");
?>
<!-- ######### HEADER END ############### -->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">File Upload</h1>
            </div>
            <div class="col-md-6">
                <div class="form-group mb-0" style="text-align: right;">
                    <button type="submit" id="btnDownload" class="btn btn-success">Demo Csv</button>
                </div>
            </div>
            <!--<div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">File Upload</a></li>
                </ol>
            </div>-->
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">File Upload</h3>
                    </div>
<?PHP
if (isset($sucMsg) && $sucMsg != '') {
    ?>
                        <div class="alert alert-<?php echo $msgClass; ?> alert-dismissable">
                            <button class="close" data-dismiss="alert">X</button>
                            <i class="fa-fw fa fa-check"></i><strong><?PHP echo $sucMsg; ?></strong> 
                        </div>
                        <?PHP
                    }
                    ?>
                    <form role="form" method="post" action="" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <!--<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Choose Company:</label>
                                        <select name="company_id" class="form-control" required>
                                            <option value="">Selected Company</option>
<?php foreach ($companyData as $comData) { ?>
                                                <option value="<?php echo $comData->id; ?>"><?php echo $comData->company_name; ?></option>
<?php } ?>
                                        </select>
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                        <label>Choose Buildings</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" name="building[]" id="b1" value="1"> Fab 2</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" name="building[]" id="b2" value="2"> Fab 35</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" name="building[]" id="b3" value="3"> Fab 7</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label><input type="checkbox" name="building[]" id="b4" value="4"> Fab 7G</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Estimate No.:</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="text" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" name="estimate_no" placeholder="Estimate No. (Integer value)" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Target:</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="number" min="1" max="20" step="1" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="form-control" name="target" placeholder="Target (mins Integer value)" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Import CSV/XLSX/XLS File:</label>
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="csvfile" id="csvfile" required>
                                                <label class="custom-file-label">Choose file</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" id="checkBtn" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->
<script>
    $(document).ready(function () {
        $('#btnDownload').on('click', (function () {
            window.location.href = 'uploadfile.php?mode=download';
        }));
        
        $('#checkBtn').click(function() {
            checked = $("input[type=checkbox]:checked").length;

            if(!checked) {
              alert("You must check at least one building.");
              return false;
            }

          });
    });
    $('.custom-file-input').on('change', function () {
        var fileName = document.getElementById("csvfile").files[0].name;
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    })
</script>