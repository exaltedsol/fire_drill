<?php
include_once '../init.php';
//$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('0')); // VALIDATION CHEK
ob_start();

/* ------ fetch client data from user table by id ---------- */
//$showVal = $general_cls_call->select_query("*", USERS, "WHERE id=:id", array(':id' => $_GET['id']), 1);

if ($_SERVER['REQUEST_METHOD'] == "POST" && (isset($_POST['btnSubmit']))) {
    extract($_POST);
    $setValues = "company_name=:company_name,employee_id=:employee_id";
    $arrayExe = array(
        ':company_name' 			=> 	$general_cls_call->specialhtmlremover($company_name),
        ':employee_id' 			=> 	$general_cls_call->specialhtmlremover($employee_id)
    );
    $whereClause = " WHERE id=" . $_GET['id'];
    $general_cls_call->update_query(USERS, $setValues, $whereClause, $arrayExe);
    $sucMsg = "Data updated successfully";
	$msgClass = "success";
}
/* ------ fetch FOUNDERS data from user table by id ---------- */
$showVal = $general_cls_call->select_query("*", USERS, "WHERE id=:id", array(':id' => $_GET['id']), 1);

if (!isset($_POST['company_name'])) {
    $_POST['company_name'] = $showVal->company_name;
}
if (!isset($_POST['employee_id'])) {
    $_POST['employee_id'] = $showVal->employee_id;
}
// ######### HEADER START ############### -->

include_once("../includes/adminHeader.php");
?>
<!-- ######### HEADER END ############### -->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit Company</h1>
            </div>
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Company</h3>
                    </div>
                    <?PHP
                    if (isset($sucMsg) && $sucMsg != '') {
                        ?>
                        <div class="alert alert-<?PHP echo $msgClass; ?> alert-dismissable">
                            <button class="close" data-dismiss="alert">X</button>
                            <i class="fa-fw fa fa-check"></i><strong><?PHP echo $sucMsg; ?></strong> 
                        </div>
					<?PHP
						}
					?>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" name="frmuser" method="post" action="" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" value="<?php echo $_POST['company_name']; ?>" name="company_name" placeholder="Company Name" required>
                                    </div>
                                </div>
								<div class="col-md-6">
                                    <div class="form-group">
                                        <label>Company Id</label>
                                        <input type="text" class="form-control" value="<?php echo $_POST['employee_id']; ?>" name="employee_id" placeholder="Company Id" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!--/.col (left) -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
<!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->			