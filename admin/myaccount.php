<?php
include_once '../init.php';
$general_cls_call->validation_check($_SESSION['ADMIN_USER_ID'], ADMIN_SITE_URL, array('1')); // VALIDATION CHEK
ob_start();

/* =========== INSERT START =========== */
		if(isset($_POST['btnSubmit']))
		{
			extract($_POST);
			$setValues="email=:email";
			$updateExecute=array(
			':email'	=>$general_cls_call->specialhtmlremover($email)
			);
			$whereClause = "WHERE 1";
			$general_cls_call->update_query(SETTINGS, $setValues, $whereClause, $updateExecute);
			$sucMsg="My account has been updated successfully....";
		}
		$where = "WHERE id=1";
		$ft = $general_cls_call->select_query("*", SETTINGS, $where, array(), 1);
/* =========== INSERT END =========== */

// ######### HEADER START ############### -->
include_once("../includes/adminHeader.php");
?>
<!-- ######### HEADER END ############### -->
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">My Account</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="users.php">My Account</a></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">My Account</h3>
                    </div>
					<?PHP
						if (isset($sucMsg) && $sucMsg != '') {
					?>
                        <div class="alert alert-success alert-dismissable">
                            <button class="close" data-dismiss="alert">X</button>
                            <i class="fa-fw fa fa-check"></i><strong>Success</strong> <?PHP echo $sucMsg; ?>
                        </div>
                    <?PHP
                    }
                    ?>
                    <form role="form" method="post" action="">
                        <div class="card-body">
                            <div class="row">
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
										<input type="email" class="form-control" id="email" name="email"" required="required" oninvalid="chkEmail(this);" oninput="chkEmail(this);" value="<?php echo $ft->email; ?>">
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="form-group">
                                        <label for="">Password</label>
										<input type="password" class="form-control" name="password" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" name="btnSubmit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ######### Footer START ############### -->
<?PHP include_once("../includes/adminFooter.php"); ?>
<!-- ######### Footer END ############### -->