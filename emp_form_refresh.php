<?php
    include_once("init.php"); 
    ini_set('max_execution_time', 0);
    $cur_date = date('Y-m-d');
    $cur_time_str = explode(':',date("H:i"));
    $cur_time = $cur_time_str[0].$cur_time_str[1];
    $data = array();
    $currentDrill = $general_cls_call->select_query("id,end_time", ATTENDANCE, "WHERE drill_date=:drill_date AND start_time!=:start_time_blank AND start_time<=:start_time AND isActive=:isActive ORDER BY id DESC", array(':drill_date'=>$cur_date, ':start_time'=>$cur_time, ':start_time_blank'=>'', ':isActive'=>1), 1);
    $id = '';
    if($currentDrill){
        if($currentDrill->end_time==''){
                $id = $currentDrill->id;
        }else{
                $drill = $general_cls_call->select_query("id", ATTENDANCE, "WHERE drill_date=:drill_date AND start_time<=:start_time AND end_time>=:end_time AND isActive=:isActive", array(':drill_date'=>$cur_date, ':start_time'=>$cur_time, ':end_time'=>$cur_time, ':isActive'=>1), 1);
                if($drill){
                        $id = $drill->id;
                }
        }
        $data['drill_id'] = $id;
        
        $buildings = $general_cls_call->select_query("building_id", BUILDINGS_PERMISSION, "WHERE drill_id=:drill_id", array(':drill_id'=>$id), 2);
        $data['buildingArr'] = $buildings;
    }
    $chakDrill = $general_cls_call->select_query("is_drill_complete", ATTENDANCE, "WHERE id=:drill_id", array(':drill_id'=>$id), 1);
    if($chakDrill->is_drill_complete!=1) {
        $data['drill_id'] = '';
    }
    echo json_encode($data);
    //echo $id;
?>